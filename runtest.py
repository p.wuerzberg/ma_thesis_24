# -*- coding: utf-8 -*-
"""
Created on Sun Feb 11 15:33:07 2024

@author: Paul

Run test to generate performance measurements compared to human data.
This is just a script to call kidney_segmenter.stats with params passed via
command line.
"""
import argparse
from pathlib import Path
from kidney_segmenter import stats



if __name__ == "__main__":

    defipath = Path('resources/iso/isotonic.joblib')
    parser = argparse.ArgumentParser(
        description="Create statistics."
    )
    parser.add_argument("-p", "--datapath", required=True, type=str,
                        help="data path for inference")
    parser.add_argument("-w", "--weightdir", required=False, type=str,
                        default="./resources/weights/10",
                        help="path to weight dir")
    parser.add_argument("-m", "--method", required=False, type=str,default='1',
                        help="'Ensemble', 'TTA', 'ISO' or 1, 2, 3")
    parser.add_argument("-tt", "--ttamodels", required=False, type=int,
                        default = 10, help="number of iterations for tta")
    parser.add_argument("-o", "--outdir", required=False, type=str,
                        default="./out", help="save results to:")
    parser.add_argument("-i","--isopath", required=False, type=str,
                    default=defipath, help="path to isotonic regression model")
    parser.add_argument("-np", "--saveasnp",
                        help="save result numpy arrays to disk",
                                                        action="store_true")
    parser.add_argument("-png", "--saveaspng",
                        help="save results as .png to disk, loses precision",
                                                        action="store_true")
    args = parser.parse_args()

    input_pt = Path(args.datapath)
    weight_path = Path(args.weightdir)

    tta = False
    iso =False
    if args.method == 'TTA'or args.method == 'tta' or args.method == '2':
        tta = True

    elif args.method == 'ISO' or args.method == 'iso' or args.method == '3':
        iso = True


    cm_path = args.isopath
    nmod = args.ttamodels
    output_dir = args.outdir
    stats.test(weight_path, input_pt, input_pt, output_dir, ISO = iso,
               TTA = tta, nmod = nmod, saveim=args.saveaspng,
               savenp=args.saveasnp, cal_model_path=cm_path)


