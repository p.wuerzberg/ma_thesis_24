# -*- coding: utf-8 -*-
"""
Created on Tue Feb 13 22:02:00 2024

@author: Paul
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy
from sklearn import metrics
import sklearn
import matplotlib.colors as mcolors
from PIL import Image
from decimal import Decimal

import matplotlib.patheffects as pe

def make_sets(true_ar, false_ar, invert=True, invert_classes=False):
    testset_id_entropy = np.array(true_ar)
    testset_ood_entropy = np.array(false_ar)

    y_true =  np.concatenate([np.ones_like(testset_id_entropy), np.zeros_like(testset_ood_entropy)])
    y_score = np.concatenate([testset_id_entropy, testset_ood_entropy])
    if invert:
        y_score = 1 - y_score
    if invert_classes:
        y_true = 1 - y_true

    return y_true, y_score


def mean_(x, dec=None):
    out = np.mean(np.array(x).ravel())
    if dec is None:
        return out
    else:
        return np.round(out, dec)
def median_(x, dec=None):
    out = np.median(np.array(x).ravel())
    if dec is None:
        return out
    else:
        return np.round(out, dec)
def make_idvood_separation_smaller(xid, xod, y, title, xname, yname, idname='', odname='', invert = True, mwu = False):
    '''

    Parameters
    ----------
    xid : array like
        Independent in domain. 1D Array.
    xod : array like
        Independent out of domain. 1D Array.
    y : array like
        Deprecated DICE. 1D Array like x.

    Returns
    -------
    None
    '''

    fig, axs = plt.subplots(1, 2, figsize=(14.5,6))
    fig.suptitle(f' {title}', fontsize=16)

    col = iter(mcolors.TABLEAU_COLORS)
    #axs[0].set_title('Distributions (normalized)')
    c = [next(col), next(col)]

    lbl_id = f'{idname}'
    lbl_od = f'{odname}'

    lbl_id_median = f'Median: {median_(xid, 3)}'
    lbl_od_median = f'Median: {median_(xod, 3)}'

    try:
        axs[0].hist([xid, xod], color=c, label = [lbl_id, lbl_od], range = [0, np.max([max(xid), max(xod)])], density=True)
    except:
        axs[0].hist([xid, xod], color=c, label = [lbl_id, lbl_od], range = [np.min([min(xid), min(xod)]), np.max([max(xid), max(xod)])], density=True)

    # axs[0].set_ylim([0, 9])
    yls ,yle = axs[0].get_ylim()
    axs[0].set_ylim(yls, yle+(.1 * (yle-yls)))
    yl, xl = axs[0].get_ylim(), axs[0].get_xlim()
    ylen = yl[1]-yl[0]
    xlen = xl[1]-xl[0]
    axs[0].axvline(median_(xid), color = 'white')
    axs[0].axvline(median_(xid), color = c[0], linestyle=':', label = lbl_id_median)
    axs[0].axvline(median_(xod), color = 'white')
    axs[0].axvline(median_(xod), color = c[1], linestyle=':', label = lbl_od_median)


    text = axs[0].text(-.05*xlen, -(.17*ylen), f'{idname}: ', ha='left')
    text = axs[0].annotate(f'{len(xid)}',
        xycoords=text, xy=(1, 0), verticalalignment="bottom", color=c[0])
    text = axs[0].annotate( f'  {odname}: ',
        xycoords=text, xy=(1, 0), verticalalignment="bottom", color="black")
    text = axs[0].annotate(f'{len(xod)}',
        xycoords=text, xy=(1, 0), verticalalignment="bottom", color=c[1])
    if mwu:
        text = axs[0].annotate(f'  = {len(xid)+len(xod)}',
            xycoords=text, xy=(1, 0), verticalalignment="bottom", color="black")


    # axs[0].text(.5*xlen, -(.2*ylen), str(scipy.stats.mannwhitneyu(xid, xod)), ha='center')
    if mwu:
        res = scipy.stats.mannwhitneyu(xid, xod)
        axs[0].text(-.05*xlen, -(.22*ylen), f'Mann-Whitney U: p = {Decimal(res.pvalue):.2e}, statistic = {res.statistic}', ha='left')
        print(scipy.stats.mannwhitneyu(xid, xod))
        print(scipy.stats.ranksums(xid, xod))

    y_true, y_score = make_sets(xid, xod)
    fpr, tpr, thresholds = metrics.roc_curve(y_true, y_score)
    roc_auc = metrics.roc_auc_score(y_true, y_score)
    #print(roc_auc, metrics.roc_auc_score(y_true, y_score))
    axs[1].plot(fpr, tpr, label = ' \nAUC: ' + str(np.round(roc_auc, 2)))
    j = thresholds[np.argmax(tpr - fpr)]
    tn, fp, fn, tp = sklearn.metrics.confusion_matrix(y_true, np.array(y_score > j, dtype='int')).ravel()
    sens = tp / (tp + fn)
    spec = tn / (fp + tn)
    xpos, ypos = fpr[np.argmax(tpr - fpr)], tpr[np.argmax(tpr - fpr)]
    axs[1].plot(xpos, ypos, 'o', c='hotpink')
    yl, xl = axs[1].get_ylim(), axs[1].get_xlim()
    ylen = yl[1]-yl[0]
    xlen = xl[1]-xl[0]
    axs[1].text(xpos+(0.01*xlen), ypos-(0.05*xlen), f'j: {np.round(1-j, 3)}')
    #plt.axvline(j, color = c, linestyle='--')
    print('tn, fp, fn, tp', tn, fp, fn, tp)
    print(' sens, spec', )
    axs[1].text(.5*xlen, -(.25*ylen), f'Sensitivity: {np.round(sens, 3)}\nSpecificity: {np.round(spec, 3)}', ha='center')
    axs[1].legend()


    yl, xl = axs[0].get_ylim(), axs[0].get_xlim()
    ylen = yl[1]-yl[0]
    xlen = xl[1]-xl[0]

    axs[0].axvline(1-j, color = 'hotpink', linestyle='--', label = 'Best Split')
    #plt.axhline(.8, color = 'pink', linestyle='--')
    print(j)

    J = 1 - j
    stroke = [pe.withStroke(linewidth=2, foreground='white')]
    axs[0].text(J-(.1*xlen), yl[1]-(.4*ylen), f'{np.sum(xid < J)}', c=c[0], ha='center', path_effects=stroke) #oben links: tp
    axs[0].text(J+(.1*xlen), yl[1]-(.4*ylen), f'{np.sum(xid >= J)}', c=c[0], ha='center', path_effects=stroke) #oben rechts: fn
    axs[0].text(J-(.1*xlen), yl[1]-(.5*ylen), f'{np.sum(xod < J)}', c=c[1], ha='center', path_effects=stroke) # unten links: fp
    axs[0].text(J+(.1*xlen), yl[1]-(.5*ylen), f'{np.sum(xod >= J)}', c=c[1], ha='center', path_effects=stroke) # unten rechts: tn


    axs[0].set_xlabel(xname)
    axs[0].set_ylabel(yname)

    handles, labels = axs[0].get_legend_handles_labels()
    print(handles, labels)
    order = [0, 2, 1, 3, 4]
    axs[0].legend([handles[idx] for idx in order],[labels[idx] for idx in order], fontsize='small')

    plt.show()
    return J, roc_auc

def reduce_pd(df):
    outlist = []
    for i, rows in df.groupby(['pid', 'set']):
        #rows = df[df['pid']==pid]
        outlist.append(rows.mean(skipna=True, numeric_only=True))
        outlist[-1]['overlap'] = np.sum(rows['overlap'])
        outlist[-1]['tumor area'] = np.sum(rows['tumor area'])
        outlist[-1]['predicted area'] = np.sum(rows['predicted area'])

        outlist[-1]['pid']=rows.iloc[0]['pid']
        outlist[-1]['set']=rows.iloc[0]['set']
        outlist[-1]['len']=len(rows)

        try:
            outlist[-1]['newdice'] = 2*outlist[-1]['overlap'] \
                / (outlist[-1]['tumor area'] + outlist[-1]['predicted area'])
        except ZeroDivisionError as e:
            if outlist[-1]['tumor area'] == 0 and outlist[-1]['predicted area'] == 0:
                outlist[-1]['newdice'] = 1.
            else:
                raise e
        outlist[-1]['MMCE'] = np.max(rows['MCE'])
        # max whatever = max(rows)

        # mean whatever = sum(row['whatever'] * row['pred area']) / sum(row[pred area])
    out_df = pd.DataFrame(outlist)
    return out_df

def join_dfs(allslices_reduced, data):
    data = data.rename(columns={'name': 'pid', 'dice': 'alldice'})
    out_df = pd.merge(allslices_reduced, data, on = ['pid', 'set'], how='outer', suffixes=['', '_stack'], copy=True, validate='one_to_one')
    return out_df

def finalize_df(df_img, df_stack):
    out_pd = reduce_pd(df_img.copy(deep=True))
    out_pd = join_dfs(out_pd, df_stack.copy(deep=True))
    return out_pd

from matplotlib.ticker import MultipleLocator
import matplotlib
def pretty_boxplt(scoredict, column=None, title = 'scores', ylbl ='', xlabels=None, colors=None, showfliers=True):
    font = {'size'   : 20}
    matplotlib.rc('font', **font)

    labels = [x for x in scoredict]
    all_data = [scoredict[x] for x in scoredict]
    #print(len(all_data), len(labels))
    fig, ax = plt.subplots(figsize=(4*len(labels), 8))
    #fig, ax = plt.subplots(figsize=(2*3, 4))
    plt.minorticks_on()
    ax.set_ylabel(ylbl)
    ax.tick_params(axis='x',which='minor',bottom=False)
    #ax.yaxis.set_minor_locator(MultipleLocator(.01))
    pos = 0
    g=plt.grid(visible=True, which='minor', axis='y', alpha=.5, linewidth=2)
    plt.grid(visible=True, which='major', axis='y', alpha=.5, linewidth=3)
    outlier_threshold = []
    for label in labels:
        if column is not None:
            data = scoredict[label][column]
        else:
            data = scoredict[label]
        if xlabels is not None:
            label = xlabels[pos]
        else:
            label = str(label)
        #print(len(label))
        if colors is not None:
            col = colors[pos]
        else:
            col = 'C0'
        pos =pos+1
        #if pos > 3: break
        bplot1 = ax.boxplot((data),
                            #vert=True,  # vertical box alignment
                            patch_artist=True,  # fill with color
                            labels=([label]),  # will be used to label x-ticks
                            showfliers=showfliers,
                            widths=.5,
                            capwidths=0.5,
                            medianprops={"color": "white", "linewidth": 2},
                            boxprops={"facecolor": col, "edgecolor": "white", "linewidth": 1},
                            whiskerprops={"color": col, "linewidth": 3},
                            capprops={"color": col, "linewidth": 3},
                            flierprops= {"marker": 'o', "markerfacecolor": col, "markersize": 8, "markeredgecolor": 'none'},
                            positions = [pos]
                           )
        for i in ['right', 'top', 'bottom', 'left']:
            plt.gca().spines[i].set_visible(False)


        tmp_mean = np.round(np.mean(data),3)
        tmp_median = np.round(np.median(data),3)
        tmp_std = np.round(np.std(data),3)
        name, median, caps, flier, boxx = label, bplot1['medians'], bplot1['caps'], bplot1['fliers'], bplot1['boxes'][0].get_path()
        #print(f'set: {name}, median: {median[0].get_data()}, caps: {caps[0].get_data()}, fliers: {flier[0].get_data()}')
        if showfliers:
            tmp_outliers = len(flier[0].get_data()[0])
        else:
            tmp_outliers = -1
        tmp_outlier_threshold = caps[0].get_data()[1][0].item()
        outlier_threshold.append(tmp_outlier_threshold)
        print(f'set: {name}, median: {np.round(median[0].get_data()[1][0].item(),3)}, caps: {np.round(caps[0].get_data()[1][0],3)}, {np.round(caps[1].get_data()[1][0],3)}, boxes: {np.round(boxx.vertices[0][1],3)}, {np.round(boxx.vertices[2][1],3)}')
        print(f'mean: {tmp_mean}, median: {tmp_median}, std: {tmp_std}, num outliers: {tmp_outliers}, outlier_threshold: {np.round(tmp_outlier_threshold,4)}')
        print(f'{tmp_mean} & {tmp_median} & {tmp_std} & {np.round(caps[0].get_data()[1][0],3)} & {np.round(boxx.vertices[0][1],3)} & {np.round(boxx.vertices[2][1],3)} & {np.round(caps[1].get_data()[1][0],3)} & {tmp_outliers}\\\\')
        print('---\n')
    ax.set_title(title)
    plt.plot()

    font = {'size'   : 15}
    matplotlib.rc('font', **font)
    return labels, outlier_threshold


def get_bounds(im, increase):
    (maxy, maxx) = im.shape
    (ylim, xlim) = np.where(im>0)

    yt = max(np.min(ylim) - increase, 0)
    yb = min(np.max(ylim) + increase, maxy-1)
    xl = max(np.min(xlim) - increase, 0)
    xr = min(np.max(xlim) + increase, maxx-1)
    return (xl,yt), xr-xl, yb-yt

import matplotlib.patches as patches
from scipy.stats import entropy
from matplotlib.colors import LinearSegmentedColormap
def visualize2(seg, inp, gt=None, title='Visualization Example', j_thr=None, j_lbl=None, entropy_bins=None, zoom=False):
    fig, axs = plt.subplots(2,2,figsize=(18,18))
    fig.suptitle(f' {title}', fontsize=16)
    x_vals = np.arange(0, 512, 1)
    y_vals = np.arange(0, 512, 1)
    X, Y = np.meshgrid(x_vals, y_vals)

    axs[0,0].set_title('Input Image (Window)')
    axs[0,0].imshow(inp, cmap='gray')
    #axs[0,0].set_xlabel('The network uses the unmodified 16 bit image.')
    if zoom is not False:
        pos, w, h = get_bounds(np.round(seg), zoom)
        rect = patches.Rectangle(pos, w, h, linewidth=1, edgecolor='r', facecolor='none')
        axs[0,0].add_patch(rect)
        seg = seg[pos[1] : pos[1]+h, pos[0] : pos[0]+w]
        inp = inp[pos[1] : pos[1]+h, pos[0] : pos[0]+w]
        gt = gt[pos[1] : pos[1]+h, pos[0] : pos[0]+w]
        x_vals = np.arange(0, w, 1)
        y_vals = np.arange(0, h, 1)
        X, Y = np.meshgrid(x_vals, y_vals)
    axs[0,1].set_title('Predicted Segment')
    axs[0,1].imshow(inp, cmap = 'gray')
    entr = 1 - entropy(np.array([seg, 1-seg]),base=2)
    seg_pred = np.round(np.copy(seg))
    entr = np.mean(entr[seg_pred==1])
    if entropy_bins is not None:
        bins = np.linspace(0,1,entropy_bins+1)
        entr_i = min([np.digitize(entr, bins)[0], entropy_bins]) - 1
        entr = bins[entr_i]
    seg_pred[seg_pred==1] = entr
    #print(np.unique(seg_pred))
    #colors = [(0, 1, .1), (0, 1, .1), (0, 1, .1)]
    #cmap = LinearSegmentedColormap.from_list('green', colors, N=2)
    cmap ='RdYlGn'
    axs01 = axs[0,1].imshow(seg_pred, cmap = 'RdYlGn', alpha=(np.round(seg)*.8), vmin=0, vmax=1)
    cbar = plt.colorbar(axs01, ax = axs[0,1], location='left',ticklocation='right', fraction=.03, aspect=30, shrink=.9, label= 'Average FG Entropy')
    lbl = np.array([str(np.round(x,1)) for x in (1 - np.linspace(0,1,6))])
    print(lbl)
    ticks=np.linspace(0,1,6)
    print(f'entropy: {np.max(seg_pred)}')
    if j_thr is not None:
        j_thr = 1-np.array(j_thr)
        for j in j_thr:
            cbar.ax.axhline(j, c='black',linestyle=':')
        ii = np.searchsorted(ticks, j_thr)
        ticks = np.insert(ticks, ii, j_thr)
        lbl = np.insert(lbl, ii, j_lbl)
    cbar.set_ticks(ticks=ticks, labels=lbl)
    cbar.ax.axhline(np.max(seg_pred), c='black',linestyle='--')

    if gt is not None:
        axs[0,1].contour(X,Y,gt, colors='black' , levels = [.5], vmin=0, vmax=1, linestyles='-', linewidths=2, alpha=1)
        axs[0,1].contour(X,Y,gt, colors='hotpink' , levels = [.5], vmin=0, vmax=1, linestyles='--', linewidths=1, alpha=1)
        #axs[0,1].set_xlabel(f'Dotted lines indicate entropy cutoff ({j_thr}) for good prediction\n(DICE overlap with human > .8)\nDashed line indicates entropy of predicted segment.\nGround truth segment outline added in red for convenience.\nThis would naturally not be available when predicting a new segment.')
    else:
        #axs[0,1].set_xlabel('')
        pass
    axs[1,0].set_title('Local Probability of Tumor')
    axs[1,0].imshow(inp, cmap = 'gray')
    entr = 1 - entropy(np.array([seg, 1-seg]),base=2)
    entr_plot = axs[1,0].imshow(seg, cmap ='RdYlGn', vmin=0, vmax=1, alpha=(entr<.99).astype('uint8')*.7)
    axs[1,0].contour(X,Y,seg, colors='lightblue' , levels = [.5], vmin=0, vmax=1, linestyles='-', linewidths=2, alpha=1)
    axs[1,0].contour(X,Y,seg, colors='black' , levels = [.5], vmin=0, vmax=1, linestyles=':', linewidths=2, alpha=1)
    if gt is not None:
        axs[1,0].contour(X,Y,gt, colors='black' , levels = [.5], vmin=0, vmax=1, linestyles='-', linewidths=2, alpha=1)
        axs[1,0].contour(X,Y,gt, colors='hotpink' , levels = [.5], vmin=0, vmax=1, linestyles='--', linewidths=1, alpha=1)
    cbar = plt.colorbar(entr_plot, ax = axs[1,0], fraction=.03, aspect=30, shrink=.9)
    #cbar.ax.axhline(.5, c='white',linestyle='--')
    #cbar.ax.axhline(.5, c='black',linestyle=':')
    axs[1,0].axvline(100, color = 'b',label='Prediction',  linestyle=':', alpha=0)
    #axs[1,0].set_xlabel('Areas with canfidence > .99 fully transparent for better visibility.')
    leg=axs[1,0].legend()
    for lh in leg.legend_handles:
        lh.set_alpha(1)
    #level = np.linspace(0,1,5)
    #axs[1,0].contour(X,Y,entr, colors = 'black', levels = level, vmin=0, vmax=1, linestyles='solid', linewidths=1)

    axs[1,1].set_title('Different Thresholds for Predicted Probability of Tumor')
    seg_contour = axs[1,1].imshow(seg, cmap ='RdYlGn', vmin=0, vmax=1, alpha=1)
    axs[1,1].axvline(100, color = 'b',label='Prediction',  linestyle=':', alpha=0)
    axs[1,1].imshow(inp, cmap='gray')

    #level = [.5]
    #level = [.01,.1,.3,.5,.7,.9,.92, .94,.96,.98,.99, .999]
    level = np.linspace(.1,1,10)
    cont11 = axs[1,1].contour(X,Y,seg, cmap ='RdYlGn', levels = level, vmin=0, vmax=1, linestyles='solid', linewidths=1, alpha=1)
    axs[1,1].contour(X,Y,seg, colors='lightblue' , levels = [.5], vmin=0, vmax=1, linestyles='-', linewidths=2, alpha=1)
    axs[1,1].contour(X,Y,seg, colors='black' , levels = [.5], vmin=0, vmax=1, linestyles=':', linewidths=2, alpha=1)
    if gt is not None:
        axs[1,1].contour(X,Y,gt, colors='black' , levels = [.5], vmin=0, vmax=1, linestyles='-', linewidths=2, alpha=1)
        axs[1,1].contour(X,Y,gt, colors='hotpink' , levels = [.5], vmin=0, vmax=1, linestyles='--', linewidths=1, alpha=1)
    #axs[1,1].set_xlabel('Thresholds in [.1,.2, ..., .9]')
    cbar=plt.colorbar(seg_contour, ax = axs[1,1], ticks=level[:-1], fraction=.03, aspect=30, shrink=.9, label= 'Predicted Probability of Tumor')
    cbar.add_lines(level,colors=['white' for x in level],linewidths=np.full(len(level), 2))
    cbar.add_lines(cont11, erase=False)
    cbar.ax.axhline(.5, c='white',linestyle='-')
    cbar.ax.axhline(.5, c='black',linestyle=':')

    leg=axs[1,1].legend()
    for lh in leg.legend_handles:
        lh.set_alpha(1)

    #axs.imshow(g[:,:,1], cmap='gray')
    #axs[0,2].imshow(inp, cmap='gray')
    #axs.contour(X,Y,seg, colors = 'hotpink', levels = level, vmin=0, vmax=1, linestyles='solid', linewidths=3, alpha=0.6)
    #axs[1].imshow(seg, cmap ='RdYlGn', alpha=.4, vmin=0, vmax=1)
    #axs.contour(X,Y,im, colors = 'black', levels = level, vmin=0, vmax=1, linestyles='solid', linewidths=1)
    for ax in axs.ravel():
        ax.set_xticks([])
        ax.set_yticks([])
    plt.show()

def get_segment(pt):
    im = np.array(Image.open(pt))
    return im[:,:,0]/255, im[:,:,1], im[:,:,2]/255


def window (image, level, width=256):
    MAX_IMAGE_VALUE = 255
    image[image < level - width/2] = level - width/2
    image[image >=  level + width/2] = level + width/2
    min_val = np.min(image)
    max_val = np.max(image)

    if max_val == min_val:
        im_norm = image /max_val
    else:
        im_norm = (image - min_val) / (max_val - min_val)

    im_bt = im_norm * MAX_IMAGE_VALUE
    # im_bt = im_bt.astype('uint8')
    return im_bt

def get_input(pt, w, l): #40,400
    im = np.array(Image.open(pt))
    out = window(im, l, w)
    return out