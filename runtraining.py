# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 11:20:24 2024

@author: Paul

Run training procedure for master thesis.
Recreates the training of the weights used to obtain the reported results.
"""
import json
from pathlib import Path
# from datetime import datetime
import argparse
from kidney_segmenter.utils import transforms
from kidney_segmenter import train_network

root_out_path = Path('./out')
PV_path = Path('./data/datasets/processed_PV/full')
ART_path = Path('./data/datasets/processed_ART/full')


# Parameters={
#     'Name': 'segmenter',
#     # High numbers that dont fit the gpu give strange errors
#     'batch_size': 64,
#     'val_batch_size': 64,
#     'epochs': 3,
#     'patience': 30,
#     'train_n_weights': 1,
#     'lr': .01,
#     # 'size': (512,512),
#     'size': (64,64),
#     'Description': 'deeplab w dice loss',
#     'date': str(datetime.now())
#     }

tr_transform = transforms.Compose([
                    transforms.to_np(),
                    # transforms.window(40+1024, 400), #L:40 +1024 offset
                    transforms.convert_mask(),
                    transforms.to_tensor(),
                    transforms.unsqueeze_tensor(target=True),
                    transforms.random_adjust_sharpness(),
                    transforms.random_elastic(),
                    transforms.random_flip(),
                    transforms.random_rotate((0,180)),
                    transforms.AddGaussianBlurr(),
                    transforms.reshape_target(),
                    transforms.three_channels()
                    ])

val_transform = transforms.Compose([
                    transforms.to_np(),
                    # transforms.window(40+1024, 400),
                    transforms.convert_mask(),
                    transforms.to_tensor(),
                    transforms.unsqueeze_tensor(),
                    transforms.reshape_target(),
                    transforms.three_channels()
                    ])

if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="Train weights."
    )
    parser.add_argument("-p", "--paramfile", required=False, type=str,
                        default="./params.json", help="path to json file")
    parser.add_argument("-tv", "--trainval",
                        help="pass train and val path instead of ART and PV",
                                                        action="store_true")
    parser.add_argument("-p1", "--path1", required=False, type=str,
        default=PV_path, help="pv path or train path if trainval flag is set")
    parser.add_argument("-p2", "--path2", required=False, type=str,
        default=ART_path, help="art path or val path if trainval flag is set")
    args = parser.parse_args()

    parameter = Path(args.paramfile)
    pv_path = Path(args.path1)
    art_path  = Path(args.path1)

    with open(parameter, "r") as outfile:
        Parameters = json.load(outfile)

    if args.trainval:
        train, val = train_network.create_dataset(pv_path, art_path)
    else:
        train, val = train_network.create_dataset_thesis(pv_path, art_path)

    train_network.training(train, val, tr_transform, val_transform, Parameters)