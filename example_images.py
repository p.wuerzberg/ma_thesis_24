# -*- coding: utf-8 -*-
"""
Created on Tue Feb 13 22:01:39 2024

@author: Paul
"""

from pathlib import Path

from visualize import utils
import argparse

pt2 = Path('./resources/test_imgs/visual/png_pred/0161/2x.png')
pt1 = Path('./resources/test_imgs/visual/input/0161/0030.png')


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="Create example images."
    )
    parser.add_argument("-i", "--input", required=False, type=str,
                        default=pt1,
                            help="path to original file")
    parser.add_argument("-p", "--prediction", required=False, type=str,
                        default=pt2,
                            help="path to prediction png with ground truth."+ \
                                " For example output of runtest with png flag")
    parser.add_argument("--zoom", required=False, type=int,
                        default=20,
                            help="Amount of padding around cutout.")

    args = parser.parse_args()

    pred = Path(args.prediction)
    orig = Path(args.input)
    pred, inp_, gt = utils.get_segment(pred)
    inp = utils.get_input(orig, 400, 40+1024)
    utils.visualize2(pred, inp, gt, j_thr=[0.477], j_lbl=['+'],
                     zoom = args.zoom)