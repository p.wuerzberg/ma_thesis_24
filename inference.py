# -*- coding: utf-8 -*-
"""
Created on Mon Feb 12 19:32:15 2024

@author: Paul
"""
import os
import argparse
from pathlib import Path
import imageio.v2 as imageio
import numpy as np

from kidney_segmenter.utils.predict_tumor_util import inferenceonly
from kidney_segmenter.utils import transforms

def save_images(img, path, name=''):
    '''
    Save list of images with float vals from 0-1 to disk at path.
    '''
    path = Path(path)
    for idx, image in enumerate(img):
        image *= 255
        imageio.imsave(Path(path , str(idx) + name + '.png'),
                       image.astype('uint8'))


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="Create statistics."
    )

    parser.add_argument("-p", "--datapath", required=True, type=str,
                        help="data path for inference")
    parser.add_argument("-w", "--weightdir", required=False, type=str,
                        default="./resources/weights/10",
                        help="path to weight dir")
    parser.add_argument("-o", "--outdir", required=False, type=str,
                        default="./out", help="save results to:")
    parser.add_argument("-s", "--size", required=False, type=int, default=512,
                        help="scale images to shape (size, size)")
    parser.add_argument("-t", "--savetype", required=False,
                    type=int,default=1,
                    help="1: save confidence map 2: binary map, 3: all models")

    args = parser.parse_args()

    size = (args.size, args.size)
    input_pt = Path(args.datapath)
    weight_path = Path(args.weightdir)
    out_path = Path(args.outdir)
    pre_transform = transforms.pre_transform(size)
    transform = transforms.Compose([
                        transforms.to_np(),
                        transforms.convert_mask(),
                        transforms.to_tensor(),
                        transforms.unsqueeze_tensor(),
                        transforms.reshape_target(),
                        transforms.three_channels()
                        ])



    os.makedirs(out_path, exist_ok = True)
    preds, probs, allout = inferenceonly(weight_path, input_pt, transform,
                                         pre_transform)

    if args.savetype == 1:
        save_images(probs, out_path)
    elif args.savetype == 2:
        save_images(preds, out_path)
    elif args.savetype == 3:
        np.savez_compressed(Path(out_path, 'preds.npy'),
                            array = np.array(allout))