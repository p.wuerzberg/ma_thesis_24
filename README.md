
# Segmentation Network for Thesis
## Description


## Requirements
`Python==3.10.13`

Packages and versions are listed in requirements.txt. It should be enough to run:
`pip install -r requirements.txt`

## How to Use the Network
There are several functions that can be run from the command line with parameters.
They are for training new weights, testing them with manual segmentations and predicting on new data.

`python ./example.py --help' shows the possible parameters.`

### Train new weights

```
python runtraining.py -tv --path1 /path/to/training/data --path2 /path/to/validation/data
```
The data should be in two folders called `img` and `mask` below with the images and masks having the same file name. The two folders should mirror each other containing folders for each scan with either images or masks below.

Options can be set in a .json file. By default `./params.json` will be loaded from the root directory.
The params.json provided is set up to train one set of weights like the ones used in the thesis.

To run the same training performed in the thesis used run:

```
python runtraining.py --path1 /path/to/PV --path2 /path/to/ART
```
The folder structure needs to look as follows. This is how the data in `/sybig/projects/UMG_Paul_Wuerzberg_Masterthesis_CT_kidney_tumor_detection/data/`
is structured as well. The training data used in the thesis can be found in `processed_ART/full/train/` and `processed_PV/full/train/`
 
    └── art
        ├── img
	│    └── patient1
        │           ├── img0.png
        │           ├── img1.png
        │           ...
        │           └── imgn.png
        └── mask
	     └── patient1
		    ├── img0.png
                    ├── img1.png
                    ...
                    └── imgn.png
    └── pv
        ├── img
        └── mask


### Test Weights
This requires a data set and a label set. The same as for training. Directory needs to be structured like above with images and targets in folders below mirrored `img` and `mask` dirs. This is how the test results were generated.

This generates `.csv` files, containing scores and different metrics. 
`allslices.csv` in the directory for each data set, contains the image wise metrics, while `data.csv` contains scan wise calculated metrics! 
To store the predictions themselves pass `-png` for `.png` images with input, prediction and ground truth in stored in the 3 color channels or `-np` for saving compressed numpy arrays with the predictions of all members and in full 32-bit float precision to disk.

``` 
python runtest.py -png -p path/to/images 
```
The number of ensemble members is determined by the number of weights in the weight dir.
The weight dir can be set with `-- weightdir` or `-w` but should point to the folder with the 10 weights used throughout the thesis from the root dir.

`-o` can be used to pass a path to store the data. By default an `out/` dir is created at the root.

To try the test on example images run the following:
``` 
python runtest.py -png -p ./resources/test_imgs/test
```

For TTA: 
```
python runtest.py -png -p path/to/images -m tta -tt 10 -w path/to/single/weight/file

```

Pointing the weight dir to a directory containing a single weight file is not strictly necessary since the first one loaded is used otherwise but the order in which they are loaded may not be clear.


### Inference on Unknown Images

```
python inference.py -t 1 -p path/to/images
```

This will save the combined confidence map for the data to `./out` or `-o out/dir`.
To generate binary predictions only pass `-t 2` and to save all member outputs as numpy arrays pass `-t 3`.

Test this on example images:
```
python inference.py -t 1 -p ./resources/test_imgs/inf 
```


## Generate Visualizations

Show example visualization for a prediction like the ones shown in the thesis. Requires a prediction output and the corresponding input image.
The default paths point to an example pair of paths. So you can just run:

```
python example_images.py
```

To generate the visualizations for arbitrary images:
```
python example_images.py -i /ct/image/file.png -p prediction/file.png
```

The file passed via `-i` is identical to those used for training and `-p` is a file with the ground truth and prediction in the color channels, like the output of `runtest.py -png`.

