# -*- coding: utf-8 -*-
"""
Created on Sun May 28 21:43:56 2023

@author: Paul
"""

from joblib import load
# from datetime import datetime
from pathlib import Path
import os
import numpy as np
from scipy.spatial import distance
from sklearn.metrics import brier_score_loss

import matplotlib.pyplot as plt
from scipy.stats import entropy
from scipy.special import rel_entr
# import ml_insights as mli
# ml insights is really fussy. It was excluded to make installing
# requirements less likely to fail.
# try installing yourself if you want or use scipy
import csv
from sklearn.metrics import roc_auc_score
# import torch
import pandas as pd
from kidney_segmenter.utils import transforms
import kidney_segmenter.utils.predict_tumor_util as p

import gc
import warnings
warnings.simplefilter("ignore", category=UserWarning)
# from utils.warn_with_trace import warn_with_traceback
# warnings.showwarning = warn_with_traceback


def test(weight_path, root_input_dir, root_mask_dir, output_dir,
         ISO = False, TTA = False, nmod = 0, saveim=False, savenp=False,
         cal_model_path=None):
    s_dicescores = []
    median_dice, mean_dice = [], []
    median_entr, mean_entr = [], []
    median_uncert, mean_uncert = [], []
    std_entr, std_dice, std_uncert = [], [], []
    brier = []
    names = []
    entropy_list = []
    stda_list = []
    all_predictions, all_gt = [], []
    tumor_sizes = []
    roc_auc_list = []
    ece_list, mce_list, pce_list= [], [], []
    entropy_fg_list, entropy_bg_list = [], []
    n_slices = []
    # all_pred_for_dice, all_gt_for_dice = [], []
    nll_list = []
    predicted_size = []

    transform = transforms.Compose([
                        transforms.to_np(),
                        transforms.convert_mask(),
                        transforms.to_tensor(),
                        transforms.unsqueeze_tensor(),
                        transforms.reshape_target(),
                        transforms.three_channels()
                        ])

    tta_transform = transforms.Compose([
                        transforms.to_np(),
                        transforms.convert_mask(),
                        transforms.to_tensor(),
                        transforms.unsqueeze_tensor(target=True),
                        transforms.random_adjust_sharpness(),
                        transforms.random_elastic(),
                        # transforms.random_flip(),
                        # transforms.random_rotate((0,180)),
                        transforms.AddGaussianBlurr(),
                        transforms.reshape_target(),
                        transforms.three_channels()
                        ])

    os.makedirs(Path(output_dir), exist_ok = True)
    c_names = ['pid','name', 'dice', 'brier', 'fg entropy', 'alt fg entropy',
               'altentr', 'bg entropy', 'entropy', 'sdev', 'tumor area',
               'predicted area', 'overlap', 'NLL', 'ECE', 'MCE', 'PCE', 'fg uncertainty',
               'max certainty', 'containsnan', 'maxval', 'medianval','avgval','max_sdev', 'mean_sdev', 'med_sdev',
               'max_disagreement', 'medi_disagreement', 'mean_disagreement',
               'std_disagreement', 'max_KL', 'medi_KL', 'mean_KL',
               'fg_mean_sdev', 'fg_med_sdev', 'fg_max_sdev', 'fg_mean_KL_mat',
               'fg_median_KL_mat', 'fg_max_KL_mat', 'fg_mean_disagreement',
               'fg_median_disagreement', 'fg_max_disagreement']


    with open(Path(output_dir,'allslices.csv'), 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',',
                        quotechar='\'', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(c_names)
    for path in os.listdir(Path(root_input_dir, 'img')):
        gc.collect()
        image = Path(root_input_dir, 'img', path)
        mask = Path(root_mask_dir, 'mask', path)
        if not os.path.exists(mask):
            print(f'not exist mask for {path}')
            continue
        os.makedirs(Path(output_dir, path), exist_ok = True)
        try:
            if not TTA:
                _, raw_s_results, all_results, div, original, gt_= \
                        p.predict2(weight_path, image, mask, transform,
                        save_file=Path(Path(output_dir, path)), modelout=2)
            else:
                _, raw_s_results, all_results, div, original, gt_ = \
                            p.tta_predict(weight_path, image, mask, tta_transform, transform, nmod)

        except RuntimeError as e:
            print(e)
            print(f'error no images loaded from {path}')
            continue

        gt_[gt_>0] = 1
        gt=gt_
        s_results =  raw_s_results/div
        if ISO:
            try:
                if cal_model_path is None:
                    cal_model_path = Path('./data/datasets/isotonic',
                                          'isotonic.joblib')
                isotonic = load(cal_model_path)
                s_results_shape = s_results.shape
                s_results = isotonic.predict(s_results.ravel())
                s_results = s_results.reshape(s_results_shape)
            except:
                with open(Path(output_dir, 'nan.txt'), 'a', newline='') as nanfile:
                    nanfile.write(str(path) +': '+ str(np.count_nonzero(np.isnan(s_results)))+'\n')
                isotonic = load(cal_model_path)
                s_results_shape = s_results.shape
                s_results = isotonic.predict(np.nan_to_num(s_results.ravel()))
                s_results = s_results.reshape(s_results_shape)
        all_res_add = np.sum(all_results, axis=0)/div

        all_predictions.append(s_results)
        all_gt.append(gt)
        ###individual slices
        std_dev = np.std(all_results[:,:,1,:,:], axis=0)
        entr = entropy(np.stack([s_results, 1-s_results]), axis=0,base=2)
        altentr  = entropy(np.stack([all_res_add[:,1,:,:], all_res_add[:,0,:,:]]), axis=0,base=2)
        max_c = np.max(all_results[:,:,1,:,:], axis=0)
        min_c = np.min(all_results[:,:,1,:,:], axis=0)
        disagreement = max_c - min_c
        # all_results[M,Batch,C,H,W], s_results [Batch,H,W], all_res_add[Batch,C,H,W]
        KL_mat=np.zeros_like(all_res_add)
        for ensemble_member in all_results:
            KL_mat =+ rel_entr(ensemble_member, all_res_add)
        KL_mat = KL_mat.sum(axis = 1)


        im_level_dice = []
        im_level_entr = []
        im_level_uncert = []
        with open(Path(output_dir, path, 'data.csv'), 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=',',
                            quotechar='\'', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(c_names)
            for idx, (slices, gt_slices, nosslices) in enumerate(zip(s_results, gt, all_res_add)):
                pid = str(path)
                name = idx
                containsnan=0
                overlap = np.sum(slices.round() * gt_slices)
                if np.sum(slices.round()) > 0:
                    dice = 1-distance.dice(slices.round().flatten(), gt_slices.flatten())
                else:
                    dice = 0.0
                im_level_dice.append(np.nan_to_num(dice))
                nll = p.NLL(slices.flatten(), gt_slices.flatten())
                try:
                    brie = brier_score_loss(gt_slices.flatten(), slices.flatten())
                except:
                    brie = np.nan
                ece, mce, pce = p.expected_calibration_error(slices.ravel(), gt_slices.ravel(), give_pce=True)
                maxval = np.max(slices)
                medianval = np.median(slices)
                avgval = np.mean(slices)
                fg_filter = slices.round() == 1
                slices_fg = slices[fg_filter]

                if np.any(slices_fg):
                    entr_foreground = np.mean(entropy(np.array([slices_fg, 1-slices_fg]),base=2))
                    uncert_fg = np.mean(np.array(slices_fg))
                    max_cert = np.max(np.array(slices_fg))
                    altentr_foreground = np.nan
                    tmp_disagreement = disagreement[idx][fg_filter]
                    fg_max_disagreement = np.max(tmp_disagreement)
                    fg_median_disagreement = np.median(tmp_disagreement)
                    fg_mean_disagreement = np.mean(tmp_disagreement)
                    tmp_KL_mat = KL_mat[idx][fg_filter]
                    fg_max_KL_mat = np.max(tmp_KL_mat)
                    fg_median_KL_mat = np.median(tmp_KL_mat)
                    fg_mean_KL_mat = np.mean(tmp_KL_mat)
                    tmp_std_dev = std_dev[idx][fg_filter]
                    fg_max_sdev = np.max(tmp_std_dev)
                    fg_med_sdev = np.median(tmp_std_dev)
                    fg_mean_sdev = np.mean(tmp_std_dev)

                else:
                    entr_foreground = np.nan
                    altentr_foreground = np.nan
                    uncert_fg = np.nan
                    max_cert = np.nan
                    containsnan=1
                    fg_max_disagreement = np.nan
                    fg_median_disagreement = np.nan
                    fg_mean_disagreement = np.nan

                    fg_max_KL_mat = np.nan
                    fg_median_KL_mat = np.nan
                    fg_mean_KL_mat = np.nan

                    fg_max_sdev = np.nan
                    fg_med_sdev = np.nan
                    fg_mean_sdev = np.nan
                im_level_entr.append(np.nan_to_num(entr_foreground))
                im_level_uncert.append(np.nan_to_num(uncert_fg))
                slices_bg = slices[slices.round() == 0]
                if np.any(slices_bg):
                    entr_background = np.mean(entropy(np.array([slices_bg, 1-slices_bg]),base=2))
                else:
                    entr_background = np.nan
                    containsnan+=2
                try:

                    mean_disagreement = np.mean(disagreement[idx])
                    medi_disagreement = np.median(disagreement[idx])
                    max_disagreement = np.max(disagreement[idx])
                    std_disagreement = np.std(disagreement[idx])
                    mean_kl = np.mean(KL_mat[idx])
                    median_kl = np.median(KL_mat[idx])
                    max_kl = np.max(KL_mat[idx])
                except:
                    mean_disagreement = np.nan
                    medi_disagreement = np.nan
                    max_disagreement = np.nan
                    std_disagreement = np.nan
                    mean_kl = np.nan
                    median_kl = np.nan
                    max_kl = np.nan

                slc_altentr = np.mean(altentr[idx])
                slc_entropy = np.mean(entr[idx])
                sdev = np.mean(std_dev[idx])
                med_sdev = np.median(std_dev[idx])
                mean_sdev = np.mean(std_dev[idx])
                max_sdev = np.max(std_dev[idx])
                tumor_area = np.sum(gt_slices.round())
                pred_area = np.sum(np.round(slices))
                writer.writerow([pid, name, dice, brie, str(entr_foreground) , str(altentr_foreground), str(slc_altentr), entr_background,
                                 slc_entropy, sdev, tumor_area, pred_area, overlap, nll, ece, mce, pce, uncert_fg, max_cert, str(containsnan),
                                 maxval, medianval, avgval,
                                 max_sdev, mean_sdev, med_sdev, max_disagreement, medi_disagreement, mean_disagreement, std_disagreement,
                                 max_kl, median_kl, mean_kl, fg_mean_sdev, fg_med_sdev, fg_max_sdev, fg_mean_KL_mat, fg_median_KL_mat,
                                 fg_max_KL_mat, fg_mean_disagreement, fg_median_disagreement, fg_max_disagreement])

        median_uncert.append(np.median(im_level_uncert))
        mean_uncert.append(np.mean(im_level_uncert))
        std_uncert.append(np.std(im_level_uncert))

        median_entr.append(np.median(im_level_entr))
        mean_entr.append(np.mean(im_level_entr))
        std_entr.append(np.std(im_level_entr))

        median_dice.append(np.median(im_level_dice))
        mean_dice.append(np.mean(im_level_dice))
        std_dice.append(np.std(im_level_dice))

        df1 = pd.read_csv(Path(output_dir,'allslices.csv'), dtype=str, index_col=False, quotechar='\'')
        df2 = pd.read_csv(Path(output_dir, path, 'data.csv'), dtype=str, index_col=False, quotechar='\'')
        df1 = pd.concat([df1,df2])
        df1.to_csv(Path(output_dir,'allslices.csv'), index=False, quotechar='\'')
        del raw_s_results, all_results, altentr
        del KL_mat, disagreement, min_c, max_c
        gc.collect()


        s_results_fg = s_results[s_results.round() == 1]
        if np.any(s_results_fg):
            entr_foreground = np.mean(entropy(np.stack([s_results_fg, 1-s_results_fg]), axis=0,base=2))
        else:
            entr_foreground = np.nan
        entropy_fg_list.append(entr_foreground)

        s_results_bg = s_results[s_results.round() == 0]
        if np.any(s_results_bg):
            entr_background = np.mean(entropy(np.stack([s_results_bg, 1-s_results_bg]), axis=0,base=2))
        else:
            entr_background = np.nan

        entropy_bg_list.append(entr_background)
        try:
            if saveim:
                p.save_layers(s_results, original, gt, Path(output_dir, str(path)))
        except Exception as e:
            print(e)
        del original
        try:
            if savenp:
                np.savez_compressed(Path(output_dir, str(path), 'nosmax.npy'),
                                    raw = np.array(all_res_add),
                                    gt = np.array(gt),
                                    smax = np.array(s_results)
                                    )

            # mli.plot_reliability_diagram(gt.flatten(), s_results.flatten(), marker='.')
            # plt.savefig(Path(output_dir, str(path),'reliability.png'))
            # plt.close('all')

        except Exception as e:
            print(e)
        plt.clf()
        try:
            roc_auc = roc_auc_score(gt.flatten(), s_results.flatten())
        except:
            roc_auc = np.nan
        roc_auc_list.append(roc_auc)
        nll_list.append(p.NLL(s_results.flatten(), gt.flatten()))
        ece, mce, pce = p.expected_calibration_error(s_results.flatten(), gt.flatten(), give_pce=True)
        ece_list.append(ece)
        mce_list.append(mce)
        pce_list.append(pce)

        try:
            b = brier_score_loss(gt.flatten(), s_results.flatten())
        except:
            b = np.nan
        brier.append(b)
        ts = np.sum(gt, axis=(1,2))
        tumor_sizes.append(np.sum(ts))
        n_slices.append(len(ts))
        s_results_thresh = s_results.round().astype('uint8')
        predicted_size.append(np.sum(s_results_thresh))
        try:
            s_dice = 1-distance.dice(s_results_thresh.flatten(), gt.flatten())
        except:
            s_dice = np.nan
        entropy_list.append(np.sum(entr)/entr.size)
        stda_list.append(np.sum(std_dev)/std_dev.size)
        s_dicescores.append(s_dice)
        names.append(path)


    gc.collect()

    # apre = np.concatenate(all_predictions).ravel()
    # agt = np.concatenate(all_gt).ravel()
    # mli.plot_reliability_diagram(agt, apre, marker='.', error_bar_alpha=.1 )
    # plt.savefig(Path(output_dir, 'reliability.png'))
    # plt.close('all')

    with open(Path(output_dir,'data.txt'), 'w') as f:
        # f.write('DICE over all images:: ' + str(overall_dice)+ '\n')
        f.write('Avg DICE over stacks using softmax:: ' + str(np.average(s_dicescores))+ '\n')
        f.write('Avg brier over stacks:: ' + str(np.average(brier))+ '\n')
        f.write('\n')
        #f.write(f'ECE, MCE, PCE : {[aece, amce, apce]} \n')
        f.write('median dice smax: ' + str(np.median(s_dicescores))+ '\n')
        f.write('median brier: ' + str(np.median(brier))+ '\n')
        f.write('median entropy: ' + str(np.median(entropy_list))+ '\n')
        f.write('median roc auc: ' + str(np.median(roc_auc_list))+ '\n')
        f.write('median ece: ' + str(np.median(ece_list))+ '\n')
        f.write('median mce: ' + str(np.median(mce_list))+ '\n')
        f.write('\n\n')
        for y,z,pt, en in zip(s_dicescores, brier, names, entropy_list):
            f.write(f"{pt}: Dice {y}, brier {z}, entropy: {en} ")
            f.write('\n\n')

    with open(Path(output_dir,'data.csv'), 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=',',
                        quotechar='\'', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(['name', 'dice', 'brier', 'entropy', 'tumor area',
                         '# slices','roc auc', 'ECE', 'MCE', 'PCE',
                         'fg entropy', 'bg entropy', 'NLL', 'predicted area',
                         'median image dice', 'mean image dice',
                         'median image entropy', 'mean image entropy',
                         'std dice', 'std entropy', 'median uncertainty',
                         'mean uncertainty','std uncertainty'])
#['pid','name', 'dice', 'brier', 'fg entropy', 'alt fg entropy', 'altentr', 'bg entropy', 'entropy',
#                 'sdev', 'tumor area', 'predicted area', 'NLL', 'ECE', 'MCE', 'PCE', 'containsnan']
        for y,z,pt, en, size, nslc, auc,ec,mc,pc, entfg, entbg, nlll, psize, \
                        meddice, meandice, medentr, meanentr, stddice, \
                        stdentr, mdu, meu, stu in zip(s_dicescores, brier, names,
                                            entropy_list, tumor_sizes, n_slices,
                                            roc_auc_list, ece_list,
                                            mce_list, pce_list,
                                            entropy_fg_list, entropy_bg_list,
                                            nll_list, predicted_size,
                                            median_dice, mean_dice, median_entr,
                                            mean_entr, std_dice, std_entr,
                                            median_uncert, mean_uncert,
                                            std_uncert):
            writer.writerow([str(pt), str(y), str(z), str(en), str(size),
                             str(nslc), str(auc), str(np.array(ec)),
                             str(np.array(mc)),str(np.array(pc)), str(entfg),
                             str(entbg), str(nlll), str(psize), str(meddice),
                             str(meandice), str(medentr), str(meanentr),
                             str(stddice), str(stdentr), str(mdu), str(meu),
                             str(stu)])

