# -*- coding: utf-8 -*-
"""
Created on Wed Jan 27 14:00:05 2021

@author: Paul
"""
from pathlib import Path
import os
import warnings
import pickle



def list_data (path, mask_path=None, sfx='.png', img_sfx=None):
    """
    Find all image paths and respective mask paths in a directory.

    Parameters
    ----------
    path : string
        Path to the root folder in wich images are found.

    Returns
    -------
    None.

    """
    if mask_path is None:
        datapath = Path(path)

    else:
        datapath = Path(mask_path)
    #print(datapath)
    img_path = []
    mask_p = []

    for dir_name, subdir_list, files in os.walk(datapath):
        for name in files:
            #print(name)
            if name.endswith(sfx) and '_mask' in name:
                #print('x')
                #mask_p.append(Path(dir_name) / name)
                if mask_path is not None:
                    im_dir = dir_name.replace(str(mask_path), str(path))
                else:
                    im_dir = dir_name
                tmp_path = Path(im_dir) / name.replace("_mask", "")
                if img_sfx is not None:
                    tmp_path=str(tmp_path).replace(str(sfx), str(img_sfx))

                if Path(tmp_path).is_file():
                    img_path.append(tmp_path)
                    mask_p.append(Path(dir_name) / name)
                else:

                    warnings.warn("File "+str(tmp_path)+" missing", ResourceWarning)
                    with open(Path('./out/missing.txt'), 'a') as f:
                        f.write(str(tmp_path)+ '\n')
    return img_path, mask_p


def list_img_mask(img_path, mask_path, sfx=None):
    """
    List all image paths and respective mask paths from two directorys.

    Parameters
    ----------
    img_path : Pathlib.Path or string
        Input images
    mask_path : Pathlib.Path or string
        label data
    sfx : str, optional
        Suffix of the images that are to be loaded. The default is '.png'.

    Returns
    -------
    img_list : list
        Input image paths as list.
    mask_list : list
        Label image paths as list

    """

    img_list = []
    mask_list = []
    # with open(Path('./out/missing_'+ str(img_path).replace('/', '-') +'.txt'), 'a') as f:
    #     f.write('images for which masks are missing')
    for dir_name, subdir_list, files in os.walk(Path(img_path)):
        for name in files:
            #print(name)
            if sfx is None or name.endswith(sfx):
                _img = (Path(dir_name) / name)
                mask_dir_name = dir_name.replace(str(img_path),
                                                 str(mask_path))
                _mask = (Path(mask_dir_name) / name)
                if _mask.is_file():
                    img_list.append(_img)
                    mask_list.append(_mask)
                else:
                    warnings.warn("File "+str(_mask)+" missing", ResourceWarning)
                    # with open(Path('./out/missing_'+ str(img_path).replace('/', '-') +'.txt'), 'a') as f:
                    #     f.write(str(_mask)+ '\n')
    return img_list, mask_list


def get_data_pv_art(pv_path, art_path, sfx = '.png'):
    """
    Get data for trainining from ART anf PV folders.
    Assumes the file structure used in the thesis:
    ├── art
    │   ├── img
    │   └── mask
    └── pv
        ├── img
        └── mask

    Parameters
    ----------
    pv_path : TYPE
        DESCRIPTION.
    art_path : TYPE
        DESCRIPTION.
    sfx : TYPE, optional
        DESCRIPTION. The default is '.png'.

    Returns
    -------
    images : TYPE
        DESCRIPTION.
    targets : TYPE
        DESCRIPTION.

    """
    pvimages, pvtargets = list_img_mask(Path(pv_path, "img"),
                                        Path(pv_path, "mask"), sfx = sfx)
    artimages, arttargets =  list_img_mask(Path(art_path, "img"),
                                           Path(art_path, "mask"), sfx = sfx)
    images, targets = pvimages + artimages, pvtargets + arttargets

    return images, targets


def create_split(img_path, mask_path, save_path, sfx=None):
    path = Path(save_path)
    img, mask = list_img_mask(img_path, mask_path, sfx)
    # c = list(zip(img, mask))
    # img, mask = zip(*c)
    # img, mask = list(img), list(mask)

    split1 = int(len(img)*.2)
    split2 = split1 + (int((len(img)-split1)*.2))
    # print(split1, split2)
    img_test = img[:split1]
    mask_test = mask[:split1]

    img_val = img[split1:split2]
    mask_val = mask[split1:split2]

    img_train = img[split2:]
    mask_train = mask[split2:]

    tlist= [img_test, mask_test, img_val, mask_val, img_train, mask_train]

    with open(path / 'img_test.pkl', 'wb') as f:
        pickle.dump(tlist, f)

def load_paths(path):
    with open(path / 'img_test.pkl', 'rb') as f:
        mynewlist = pickle.load(f)

        return mynewlist
def list_images (path, sfx='.png'):
    """
    Find all image paths in a directory. No masks.

    Parameters
    ----------
    path : string
        Path to the root folder in wich images are found.

    Returns
    -------
    img_path: list
        list of pathlib.Path objects

    """

    img_path = []

    for dir_name, _, files in os.walk(path):
        for name in files:
            if name.endswith(sfx):
                img_path.append(Path(dir_name) / name)

    return img_path