# -*- coding: utf-8 -*-
"""
Add stack trace to warnings for debug.

Just do:
    from warn_with_trace import warn_with_traceback
    warnings.showwarning = warn_with_traceback

"""

import traceback
import warnings
import sys

def warn_with_traceback(message, category, filename, lineno, file=None, line=None):

    log = file if hasattr(file,'write') else sys.stderr
    traceback.print_stack(file=log)
    log.write(warnings.formatwarning(message, category, filename, lineno, line))