# -*- coding: utf-8 -*-
"""
Created on Fri May 26 11:42:40 2023

@author: Paul
"""
# import click
import imageio.v2 as imageio
import os
from pathlib import Path
import torch
import torch.nn.functional as F
import numpy as np
import torch.nn as nn
import cv2
import matplotlib.pyplot as plt
from sklearn.metrics import brier_score_loss
from scipy.spatial import distance
from PIL import Image
from scipy.special import xlogy
import torchvision.transforms.functional as TF

from kidney_segmenter.segmenter import premade
from kidney_segmenter.utils.utils import get_device
from kidney_segmenter.utils.tumordataset import SegmentationDataSet
# from kidney_segmenter.utils import list_data
from kidney_segmenter.utils import transforms

# SIZE = (64, 64)
SIZE = (512, 512)

def inferenceonly(weight_path, input_dir, transform, pre_transform, modelout=2):
    # pre_transform = transforms.pre_transform(SIZE)
    # pre_transform = transforms.pre_transform((64,64))
    weight_path = Path(weight_path)
    img = sorted([Path(input_dir,img) for img in os.listdir(input_dir)])

    data_set = SegmentationDataSet(img, img, use_cache = False,
                                   pre_transform=pre_transform, transform=transform)
    loader = torch.utils.data.DataLoader(data_set, batch_size = 64,
                                         shuffle=False)
    sigmoid = nn.Sigmoid()
    device = get_device()
    weights = get_weights(weight_path, device)
    resultlistlist = []
    lsmax_results = []
    lresults = []
    limages = []

    with torch.no_grad():
        for images, _ in loader:
            smax_results = np.zeros(shape=images.shape)[:,1,:,:]
            result_list = []
            limages.append(images)
            for weight in weights:
                model = get_model(weight, device, outdim=modelout)
                model.eval()
                predictions = model(images.to(device))
                smax_results += \
                        sigmoid(predictions['out'][:,1,:,:]).cpu().numpy()
                result_list.append(
                    sigmoid(predictions['out'][:,1,:,:]).cpu().numpy())
            smax_results = smax_results/len(weights)
            lsmax_results.append(smax_results)
            lresults.append(np.round(smax_results))
            resultlistlist.append(np.stack(result_list))

    return np.concatenate(lresults), np.concatenate(lsmax_results), \
            np.concatenate(resultlistlist, axis=1)

def predict2(weight_path, input_dir, mask_dir, transform, save_file = None,
             modelout=2):
    pre_transform = transforms.pre_transform(SIZE)
    # pre_transform = transforms.pre_transform((64,64))
    weight_path = Path(weight_path)
    img = sorted([Path(input_dir,img) for img in os.listdir(input_dir)])
    mask = sorted([Path(mask_dir,img) for img in os.listdir(mask_dir)])
    if save_file is not None:
        with open(Path(save_file, 'order.txt'), 'w', newline='\n') as file:
            file.writelines([str(x)+' : '+str(y)+'\n' for x,y in zip(img,
                                                                     mask)])


    data_set = SegmentationDataSet(img, mask, use_cache = False,
                            pre_transform=pre_transform, transform=transform)
    loader = torch.utils.data.DataLoader(data_set, batch_size = 64,
                                         shuffle=False)
    sigmoid = nn.Sigmoid()
    device = get_device()
    weights = get_weights(weight_path, device)
    resultlistlist = []
    lsmax_results = []
    lresults = []
    limages = []
    mask_lst = []

    with torch.no_grad():
        for images, masks in loader:
            smax_results = np.zeros(shape=images.shape)[:,1,:,:]
            results = np.zeros(shape=images.shape)[:,1,:,:]
            result_list = []
            limages.append(images)
            mask_lst.append(masks)
            for weight in weights:
                model = get_model(weight, device, outdim=modelout)
                model.eval()

                predictions = model(images.to(device))
                if modelout == 1:
                    predictions['out'] = torch.cat([predictions['out'],
                                                predictions['out']], axis=1)
                # smax_results += F.softmax(predictions['out'], dim=1)[:,1,:,:].cpu().numpy()
                smax_results += sigmoid(predictions['out'][:,1,:,:]).cpu().numpy()
                results += torch.argmax(predictions['out'], dim=1).cpu().numpy()
                result_list.append(sigmoid(predictions['out']).cpu().numpy())
            lsmax_results.append(smax_results)
            lresults.append(results)
            resultlistlist.append(np.stack(result_list))
    #inter observer agreement
    return np.concatenate(lresults), np.concatenate(lsmax_results), \
            np.concatenate(resultlistlist, axis=1), len(weights),  \
            np.concatenate(limages), np.concatenate(mask_lst)[:,1,:,:].astype('uint8')



def tta_predict(weight_path, input_dir, mask_dir, transform, image_transform, n):
    pre_transform = transforms.pre_transform(SIZE)
    weight_path = Path(weight_path)
    img = sorted([Path(input_dir,img) for img in os.listdir(input_dir)])
    mask = sorted([Path(mask_dir,img) for img in os.listdir(mask_dir)])
    data_set = SegmentationDataSet(img, mask, use_cache = False, pre_transform=pre_transform, transform=None)
    loader = torch.utils.data.DataLoader(data_set, batch_size = 64, shuffle=False)
    device = get_device()
    weights = get_weights(weight_path, device)
    resultlistlist = []
    lsmax_results = []
    lresults = []
    limages = []
    mask_lst = []
    sigmoid = nn.Sigmoid()

    with torch.no_grad():
        model = get_model(weights[0], device)
        model.eval()
        for images, masks in loader:
            smax_results = np.zeros(shape=images.shape)
            results = np.zeros(shape=images.shape)
            result_list = []
            mask_lst.append(masks)
            limages.append(transformation(images, image_transform))
            for _ in range(0, n):
                img = transformation(images, transform)
                predictions = model(img.to(device))
                smax_results += sigmoid(predictions['out'][:,1,:,:]).cpu().numpy()
                results += torch.argmax(predictions['out'], dim=1).cpu().numpy()
                result_list.append(sigmoid(predictions['out']).cpu().numpy())
            lsmax_results.append(smax_results)
            lresults.append(results)
            resultlistlist.append(np.stack(result_list))
    #inter observer agreement
    return np.concatenate(lresults), np.concatenate(lsmax_results), \
            np.concatenate(resultlistlist, axis=1), n, np.concatenate(limages),\
            np.concatenate(mask_lst).astype('uint8')

def tta_predict_better(weight_path, input_dir, mask_dir, transform, image_transform, n):
    pre_transform = transforms.pre_transform(SIZE)
    weight_path = Path(weight_path)
    img = sorted([Path(input_dir,img) for img in os.listdir(input_dir)])
    mask = sorted([Path(mask_dir,img) for img in os.listdir(mask_dir)])
    data_set = SegmentationDataSet(img, mask, use_cache = False, pre_transform=pre_transform, transform=None)
    loader = torch.utils.data.DataLoader(data_set, batch_size = 64, shuffle=False)
    device = get_device()
    weights = get_weights(weight_path, device)
    resultlistlist = []
    lsmax_results = []
    lresults = []
    limages = []
    mask_lst = []
    sigmoid = nn.Sigmoid()

    with torch.no_grad():
        model = get_model(weights[0], device)
        model.eval()
        for images, masks in loader:
            smax_results = np.zeros(shape=images.shape)
            results = np.zeros(shape=images.shape)
            result_list = []
            mask_lst.append(masks)
            limages.append(transformation(images, image_transform))
            for _ in range(0, n):
                img, trans_param = transformation_reversible(images, transform)
                predictions = model(img.to(device))
                predictions = reverse_transformation(predictions['out'], trans_param)
                smax_results += sigmoid(predictions[:,1,:,:]).cpu().numpy()
                results += torch.argmax(predictions, dim=1).cpu().numpy()
                result_list.append(sigmoid(predictions).cpu().numpy())
            lsmax_results.append(smax_results)
            lresults.append(results)
            resultlistlist.append(np.stack(result_list))
    #inter observer agreement
    return np.concatenate(lresults), np.concatenate(lsmax_results), \
            np.concatenate(resultlistlist, axis=1), n, np.concatenate(limages),\
            np.concatenate(mask_lst).astype('uint8')
def transformation(images, transform):
    outlist = []
    for im in images:
        out, _ = transform(im, im)
        outlist.append(out)
    return torch.stack(outlist)

def transformation_reversible(images, transform):
    outlist = []
    param_list = []
    for im in images:
        out, _, params = transform(im, im)
        outlist.append(out)
        param_list.append(params)
    return torch.stack(outlist), param_list

def reverse_transformation(images, param_list):
    outlist = []
    for image, (flip, angle) in zip(images, param_list):
        image = TF.rotate(image, angle * (-1), expand=False, fill=0)
        if flip == 1:
            image = TF.hflip(image)
        outlist.append(image)
    return torch.stack(outlist)

def predict(weight_path, input_dir):
    weight_path = Path(weight_path)
    images = get_images(input_dir)
    size = list(images.shape)
    size = (size[0],size[2], size[3])
    smax_results = np.zeros(shape=size)
    results = np.zeros(shape=size)
    device = get_device()
    weights = get_weights(weight_path, device)
    result_list = []
    for weight in weights:
        model = get_model(weight, device)
        model.eval()
        with torch.no_grad():
            predictions = model(images.to(device))
            smax_results += F.softmax(predictions['out'], dim=1)[:,1,:,:].cpu().numpy()
            results += torch.argmax(predictions['out'], dim=1).cpu().numpy()
            result_list.append(predictions['out'].cpu().numpy())
    #inter observer agreement
    return results, smax_results, np.stack(result_list), len(weights), images


def get_images(path):
    image_list = []
    path = Path(path)
    for image in os.listdir(path):
        im = imageio.imread(path / image)
        im = np.array(Image.fromarray(im).resize(SIZE))
        # im = np.array(Image.fromarray(im).resize((64,64)))
        image_list.append(prepare_image(im))
    return torch.stack(image_list).float()

def prepare_image(image):
    #np.array?
    # image = window(image, 40+1024, 400)
    image = torch.from_numpy(image.astype('float32')).type(torch.float32)
    if torch.min(image) == torch.max(image):
        image = image / torch.max(image)
    else:
        image = (image-torch.min(image))/(torch.max(image)-torch.min(image))
    image = torch.unsqueeze(image, 0)
    image = image.expand(3,*image.shape[1:])
    return image

def get_model(weight, device, outdim=2):
    model = premade.model_deeplab(outdim)
    model =model.to(device)
    model = nn.DataParallel(model)
    model.load_state_dict(weight)
    return model


def get_gt(path):
    image_list = []
    path = Path(path)
    pathlist = sorted([Path(path , img) for img in os.listdir(path)])
    for image in pathlist:
        im = imageio.imread(path / image)
        im = np.array(Image.fromarray(im).resize(SIZE))
        image_list.append(im)
    return np.stack(image_list)


def normalize(image):
    min_val = np.min(image)
    max_val = np.max(image)

    if max_val == min_val:
        im_norm = image /max_val
    else:
        im_norm = (image - min_val) / (max_val - min_val)

    return im_norm


def window (image, level, width=256):
    MAX_IMAGE_VALUE = 255
    image[image < level - width/2] = level - width/2
    image[image >=  level + width/2] = level + width/2
    min_val = np.min(image)
    max_val = np.max(image)

    if max_val == min_val:
        im_norm = image /max_val
    else:
        im_norm = (image - min_val) / (max_val - min_val)

    im_bt = im_norm * MAX_IMAGE_VALUE
    # im_bt = im_bt.astype('uint8')
    return im_bt

def get_weights(path, device):
    weight_list = []
    for file in os.listdir(path):
        weight_list.append(torch.load(path / file, map_location=device))
    return weight_list

def save_masks(mask, path, name=''):
    path = Path(path)
    for idx, image in enumerate(mask):
        image = image*65535
        imageio.imsave(Path(path , str(idx) + name + '.png'), image.astype('uint16'))

def save_hist(values, path, name=''):
    path = Path(path)
    plt.hist(values.flatten(), bins = 30)
    plt.savefig(Path(path,  name + '.png'))
    plt.clf()

def save_layers(mask, orig, gt, path):
    path = Path(path)
    for idx, (image, original, groundtruth) in enumerate(zip(mask, orig, gt)):
        image = image*255
        # original_ = cv2.normalize(np.array(original), None, 255, 0, cv2.NORM_MINMAX, cv2.CV_8U)
        # following broke this for tta maybe still needed
        original_ = cv2.normalize(np.array(original[0]), None, 255, 0, cv2.NORM_MINMAX, cv2.CV_8U)
        groundtruth = cv2.normalize(groundtruth, None, 255, 0, cv2.NORM_MINMAX, cv2.CV_8U)
        out = np.array([image, original_, groundtruth])
        out = np.moveaxis(out,0,2)
        imageio.imsave(Path(path , str(idx) + 'x.png'), out.astype('uint8'))

def save_plot(mask, orig, path):
    path = Path(path)
    for idx, (image, original) in enumerate(zip(mask, orig)):
        fig, axs = plt.subplots(1, 2)
        axs[0].imshow(image)
        original_ = cv2.normalize(np.array(original[0]), None, 255, 0, cv2.NORM_MINMAX, cv2.CV_8U)
        axs[1].imshow(original_, cmap='gray')
        plt.savefig(Path(path, str(idx)+'plt.png'))
        plt.clf()

def save_plot_gt(mask, orig, gt, path):
    path = Path(path)
    for idx, (image, original, groundtruth) in enumerate(zip(mask, orig, gt)):
        fig, axs = plt.subplots(1, 3, figsize=(15,5))
        results = image.round().astype('uint8')
        dice = 1-distance.dice(results.flatten(), groundtruth.flatten())
        brier = brier_score_loss(groundtruth.flatten(), image.flatten())
        fig.suptitle(f'brier: {np.round(brier, 5)}, dice: {np.round(dice,5)}')
        axs[0].imshow(image, vmin=0, vmax=1)
        axs[0].set_title('Prediction')
        axs[1].imshow(groundtruth)
        axs[1].set_title('Ground Truth')
        axs[2].imshow(original[0], cmap='gray', vmin=0, vmax=1)
        axs[2].set_title('Input')
        plt.savefig(Path(path, str(idx)+'plt.png'))
        plt.close('all')


def expected_calibration_error(samples, true_labels, M=15, give_pce=False):
    # uniform binning approach with M number of bins
    bin_boundaries = np.linspace(0, 1, M + 1)
    bin_lowers = bin_boundaries[:-1]
    bin_uppers = bin_boundaries[1:]

   # keep confidences / predicted "probabilities" as they are
    confidences = samples
    # get binary class predictions from confidences
    # predicted_label = (samples>0.5).astype(float)

    # get a boolean list of positive predictions
    accuracies = true_labels==1
    bin_lowers[0] = -1e-6
    ece = 0
    mce = 0
    pce = 0
    for bin_lower, bin_upper in zip(bin_lowers, bin_uppers):
        # determine if sample is in bin m (between bin lower & upper)
        in_bin = np.logical_and(confidences > bin_lower.item(), confidences <= bin_upper.item())
        # can calculate the empirical probability of a sample falling into bin m: (|Bm|/n)
        prop_in_bin = in_bin.astype(float).mean()

        if prop_in_bin.item() > 0:
            # get the accuracy of bin m: acc(Bm)
            accuracy_in_bin = accuracies[in_bin].astype(float).mean()
            # get the average confidence of bin m: conf(Bm)

            avg_confidence_in_bin = confidences[in_bin].mean()
            # calculate |acc(Bm) - conf(Bm)| * (|Bm|/n) for bin m and add to the total ECE
            bin_error = np.abs(accuracy_in_bin - avg_confidence_in_bin)
            ece += bin_error * prop_in_bin
            pce += bin_error * 1/M
            mce = max(bin_error, mce)
    if give_pce:
        return ece, mce, pce
    else:
        return ece, mce

def NLL(Xpred, y):
    n = len(y)
    np.clip(Xpred, a_min=1e-6, a_max=1-1e-6, out=Xpred)
    loss = -(xlogy(y, Xpred) + xlogy(1 - y, 1 - Xpred)).sum()
    loss /= n
    return loss


