import torch


from torch.utils import data

from tqdm import tqdm
from pathlib import Path
import os
import numpy as np
import imageio

#import cv2

import gc

class SegmentationDataSet(data.Dataset):

    def __init__(self,
                 inputs=None,
                 targets=None,
                 transform=None,
                 use_cache=False,
                 data=None,
                 inp_loader=imageio.imread,
                 tar_loader=imageio.imread,
                 pre_transform=None,
                 to_three_channels=False
                 ):
        self.inputs = inputs
        self.targets = targets
        self.inp_loader = inp_loader
        self.tar_loader = tar_loader
        self.transform = transform
        self.inputs_dtype = torch.float32
        self.targets_dtype = torch.long
        self.use_cache = use_cache
        self.pre_transform = pre_transform
        self.to_three_channels = to_three_channels
        self.cached_data_in = []
        self.cached_data_targ = []

        if data is not None:            # should be obsolete now
            self.cached_data_in = data[0]
            self.cached_data_targ = data[1]
            self.use_cache = True
        elif self.use_cache and self.inputs is not None:
            self.load_images()
        gc.collect()

    def load_images(self):

        progressbar = tqdm(range(len(self.inputs)), desc='Caching')
        for i, img_name, tar_name in zip(progressbar, self.inputs,
                                         self.targets):
            img = self.inp_loader(img_name)
            tar = self.inp_loader(tar_name)

            if self.pre_transform is not None:

                img, tar = self.pre_transform(img, tar)
            self.cached_data_in.append(img)
            #print('nach:',np.amax(np.asarray(img)))
            self.cached_data_targ.append(tar)
    def __len__(self):
        if self.use_cache:
            return len(self.cached_data_in)
        else:
            return len(self.inputs)

    def __getitem__(self, index: int):
 #       N_CLASSES = 2

        if self.use_cache:
            x = self.cached_data_in[index]
            y = self.cached_data_targ[index]
        else:
            # Select the sample
            input_ID = self.inputs[index]
            target_ID = self.targets[index]
            img = self.inp_loader(input_ID)
            tar = self.inp_loader(target_ID)
            # Load input and target
            if self.pre_transform is not None:
               x, y = self.pre_transform(img, tar)

        # Processing
        if self.transform is not None:
            x, y = self.transform(x, y)
        # if self.to_three_channels:
        #     x = np.array([x,x,x])
        #     y = np.moveaxis(y, -1, 0)


        # some bs to make the shapes fit this is quick and dirty
        #muss alles nach transforms!!!
        # x = np.expand_dims(x,-1)
        # x = np.moveaxis(x,-1,0)
        # y = np.moveaxis(y,-1,0)
        # Typecasting
        # x, y = torch.from_numpy(x.astype(int)).type(self.inputs_dtype), \
        #         torch.from_numpy(y.astype(int)).type(self.targets_dtype)
        #x, y = torch.from_numpy(x).type(self.inputs_dtype), torch.from_numpy(y).type(self.targets_dtype)
        # if x.ndim == 2:
        #     x = x.unsqueeze(0)
        # if y.ndim == 2:
        #     #y = y.unsqueeze(0)

        #     try:
        #         #y = F.one_hot(y,num_classes=N_CLASSES)
        #         y = F.one_hot(y,num_classes=N_CLASSES).permute(2,0,1)
        #     except:
        #         print('max: ', np.amax(y))
        #         print('all: ', np.unique(y))
        #         raise Exception("Class values must be smaller than num_classes.")
        return x, y

    #Warum existiert list_data() hier und in list_data.py? wird das hier irgendwo benutzt?
    # def list_data (path):
    #     """
    #     Find all image paths and respective mask paths in a directory.

    #     Parameters
    #     ----------
    #     path : string
    #         Path to the root folder in wich images are found.

    #     Returns
    #     -------
    #     None.

    #     """
    #     datapath = Path(path)

    #     img_path = []
    #     mask_path = []

    #     for dir_name, subdir_list, files in os.walk(datapath):
    #         for name in files:
    #             # print(name)
    #             if name.endswith('.png') and '_mask' in name:
    #                 mask_path.append(Path(dir_name) / name)
    #                 img_path.append(Path(dir_name) / name.replace("_mask", ""))
    #     return img_path, mask_path

    # def save_to_file(self, path, name='data'):
    #     """
    #     Save the loaded data as a numpy array to the disk since loading this
    #     array is much faster than loading the individual files.

    #     Parameters
    #     ----------
    #     path : string
    #         The directory where the np array is to be saved.
    #     name : string, optional
    #         prefix for the filename under wich the array will be saved. The default is 'data'.

    #     Returns
    #     -------
    #     None.

    #     """
    #     im_path = os.path.join(path, str(name + '_imgs.npy'))
    #     ta_path = os.path.join(path, str(name + '_targ.npy'))
    #     np_imgs = np.array(self.cached_data_in)
    #     np_targ = np.array(self.cached_data_targ)
    #     np.save(im_path, np_imgs)
    #     np.save(ta_path, np_targ)

    # def load_from_file(self, path, name='data'):
    #     im_path = os.path.join(path, str(name + '_imgs.npy'))
    #     ta_path = os.path.join(path, str(name + '_targ.npy'))
    #     self.cached_data_in = np.load(im_path, allow_pickle=True)[()]
    #     self.cached_data_targ = np.load(ta_path, allow_pickle=True)[()]
    #     self.use_cache = True

