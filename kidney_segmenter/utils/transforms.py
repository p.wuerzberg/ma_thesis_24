# -*- coding: utf-8 -*-
"""
Created on Tue Feb  9 18:05:40 2021

@author: Paul

"""
from PIL import Image
import numpy as np
import torch
import torchvision
import torchvision.transforms.functional as TF
import random
import torch.nn.functional as F


def get_pre_transform(shape):
    return torchvision.transforms.Resize(shape)

random.seed(42)

class transform(object):

    def __init__(self, output_size = 0, gaussian = 0.0,
                 normal_transform=None, transform_list=None):
        """
        Init object for transformations.

        Parameters
        ----------
        output_size : int
            To rescale the image to output_size*output_size.
            If output_size is 0 the size will not be changed
        gaussian : float
            Gauss parameter. Nothing happens if gaussian is 0.
            The default is 0.
        normal_transform : torch.Transform
            Transform for only the image (normalize, ...)
        transform_list : torch.Transform
            Transform object (torchvision.transforms.compose) that has
            transforms for image AND target

        Returns
        -------
        None.

        """
        self.transform = normal_transform
        self.transform_list = transform_list
        self.output_size = output_size
        if output_size != 0:
            self.resize = torchvision.transforms.Resize(output_size,
                                                interpolation='bicubic')
        self.gauss = None
        if gaussian != 0.0:
            self.gauss = torchvision.transforms.GaussianBlur(gaussian)

    def __call__(self, image, target):
        """
        Call object with image, target to transform them.
        This is intended for training and not for validation
        so the output should have the same properties as the input

        Parameters
        ----------
        image : torch.tensor
            DESCRIPTION.
        target : torch.tensor
            DESCRIPTION.

        Returns
        -------
        image : torch.tensor
            DESCRIPTION.
        target : torch.tensor
            DESCRIPTION.

        """

        if self.transform is not None:
            image = self.transform(image)
        image = Image.fromarray(image.astype('int32'),mode='I')
        target = Image.fromarray(target)
        if self.output_size != 0:
           #image = self.resize(image)
           #target = self.resize(target)
           pass
        if self.gauss is not None and random.random() > 0.5:
            image = self.gauss(image)
        if random.random() > 0.5:
            image = TF.hflip(image)
            target = TF.hflip(target)
        if random.random() > 0.5:
            image = TF.vflip(image)
            target = TF.vflip(target)
        if random.random() > 0.5:
            angle = random.randint(-45, 45)
            image = TF.rotate(image, angle)
            target = TF.rotate(target, angle, fill=0)

        if self.transform_list is not None:
            image = self.transform_list(image)
            target = self.transform_list(target)
            #target = self.transform(target)
        image, target = np.array(image), np.array(target)

        return image, target


class AddGaussianNoise(object):
    def __init__(self, mean=0., std=1.):
        """
        Transformation to add gaussian noise to a tensor.

        Parameters
        ----------
        mean : TYPE, optional
            Mean of the gaussian distribution. The default is 0..
        std : TYPE, optional
            Standard deveation of the distribution. The default is 1..

        Returns
        -------
        None.

        """
        self.std = std
        self.mean = mean

    def __call__(self, image, target):
        """
        Add noise to the passed nparray.

        Parameters
        ----------
        tensor : torch.tensor()
            Tensor to be transformed.

        Returns
        -------
        torch.tensor()
            Return the tensor with added noise.

        """
        return image + np.random.normal(self.mean, self.std, image.shape), \
                                                                        target

    def __repr__(self):
        return self.__class__.__name__ + '(mean={0}, std={1})'.format(
                                                        self.mean, self.std)


class AddGaussianBlurr(object):
    def __init__(self, p=.2, sigma=.2, kernel=5):
        self.gauss = torchvision.transforms.GaussianBlur(kernel, sigma)
        self.p = p
    def __call__(self, image, target):
        if self.p < random.random():
            image = self.gauss(image)
        return image, target




class pre_transform(object):

    def __init__(self, output_size=None, colordepth='256'):
        if output_size is not None:
            self.resize = torchvision.transforms.Resize(output_size)
        self.output_size = output_size
        self.colordepth = colordepth

    def __call__(self, image, target):
        """
        Transform original and target images.
        This is intended as transformation while loading.

        Parameters
        ----------
        image : numpy array
            2D np array of the original to be transformed
        target : numpy array
            2D np array of the target to be transformed.

        Returns
        -------
        image_out : numpy array
            transformed image.
        target_out : numpy array
            transformed target.

        """

        image = Image.fromarray(image.astype('int32'),mode='I')
        target = Image.fromarray(target,mode='L')

        #image = np.array(self.resize(image.convert('RGB',
         #                                          colors=self.colordepth)))
        #target = np.array(self.resize(target.convert('L')))
        if self.output_size is not None:
            image = self.resize(image)
            target = self.resize(target)
        image = np.array(image)
        target = np.array(target)
        #print(image.shape)
        #print(target.shape)
        #target[:,:,2][target[:,:,2] == target[:,:,1]] = 0
        #target[:,:,2][target[:,:,2] == target[:,:,0]] = 0
        #target[:,:,0][target[:,:,0] == target[:,:,1]] = 0
        return image, target

class pre_transform_rgb(object):

    def __init__(self, output_size=None):
        if output_size is not None:
            self.resize = torchvision.transforms.Resize(output_size)
        self.output_size = output_size


    def __call__(self, image, target):
        """
        Transform original and target images.
        This is intended as transformation while loading.

        Parameters
        ----------
        image : numpy array
            2D np array of the original to be transformed
        target : numpy array
            2D np array of the target to be transformed.

        Returns
        -------
        image_out : numpy array
            transformed image.
        target_out : numpy array
            transformed target.

        """

        #image = Image.fromarray(image)
        #target = Image.fromarray(target)

        #image = np.array(self.resize(image.convert('RGB',
         #                                          colors=self.colordepth)))
        #target = np.array(self.resize(target.convert('L')))
        if self.output_size is not None:
            image = self.resize(image)
            target = self.resize(target)
        image = np.array(image)
        target = np.array(target)
        #target[:,:,2][target[:,:,2] == target[:,:,1]] = 0
        #target[:,:,2][target[:,:,2] == target[:,:,0]] = 0
        #target[:,:,0][target[:,:,0] == target[:,:,1]] = 0
        return image, target


class resize(object):

    def __init__(self, output_size):
        self.resize = torchvision.transforms.Resize(output_size)

    def __call__(self, image, target):
        image = self.resize(image)
        target = self.resize(target)

        return image, target


class random_crop(object):

    def __init__(self, size):
       self.im_ht = size
       self.im_wd = size

    def __call__(self, image, target):
        i, j, h, w = torchvision.transforms.RandomCrop.get_params(image,
                                    output_size=(self.im_ht, self.im_wd))
        image = TF.crop(image, i, j, h, w)
        target = TF.crop(target, i, j, h, w)

        return image, target


class random_rotate(object):

    def __init__(self, degrees=(-20,20)):
       self.degrees=degrees

    def __call__(self, image, target):
        angle = torchvision.transforms.RandomRotation.get_params(degrees=
                                                                 self.degrees)
        image = TF.rotate(image, angle, expand=False, fill=0)
        target = TF.rotate(target, angle, expand=False, fill=0)

        return image, target

class random_rotate_r(object):

    def __init__(self, degrees=(-20,20)):
       self.degrees=degrees

    def __call__(self, image, target):
        angle = torchvision.transforms.RandomRotation.get_params(degrees=
                                                                 self.degrees)
        image = TF.rotate(image, angle, expand=False, fill=0)
        target = TF.rotate(target, angle, expand=False, fill=0)

        return image, target, angle

class random_perspective(object):

    def __init__(self, size=(64,64), p=.5, scale = .1):
       self.p = p
       self.scale = scale
       self.height = size[1]
       self.width = size[0]

    def __call__(self, image, target):
        if self.p < random.random():
            l1, l2 = torchvision.transforms.RandomPerspective.get_params(
                        self.width, self.height, distortion_scale=self.scale)

            image = TF.perspective(image, l1, l2, fill=0)
            target = TF.perspective(target, l1, l2, fill=0)

        return image, target


class random_elastic(object):

    def __init__(self, alpha=50.0, sigma=5.0, p=.5):
       self.elastic_transformer = torchvision.transforms.ElasticTransform(
           alpha, sigma)
       self.p = p
    def __call__(self, image, target):
        if self.p < random.random():
            image = self.elastic_transformer(image)

        return image, target

class random_flip(object):

    def __init__(self, p=.5):
       self.p = p

    def __call__(self, image, target):
        if random.random() < self.p:
            image = TF.hflip(image)
            target = TF.hflip(target)

        return image, target

class random_flip_r(object):

    def __init__(self, p=.5):
       self.p = p

    def __call__(self, image, target):
        flip = 0
        if random.random() < self.p:
            image = TF.hflip(image)
            target = TF.hflip(target)
            flip = 1

        return image, target, flip

class random_adjust_sharpness(object):

    def __init__(self,factor=2, p=.5):
       self.sharpness_adjuster = torchvision.transforms.RandomAdjustSharpness(
                                               sharpness_factor=factor, p=p)

    def __call__(self, image, target):

        image = self.sharpness_adjuster(image)

        return image, target



# class to_pil(object):

#     def __init__(self):
#         self.topil = torchvision.transforms.ToPILImage()

#     def __call__(self, image, target):
#         image = self.topil(image.astype('float'))
#         target = self.topil(target.astype('float'))
#         return image, target


# class to_tensor(object):

#     def __init__(self):
#         self.totensor = torchvision.transforms.ToTensor()

#     def __call__(self, image, target):
#         image = self.totensor(image)
#         target = self.totensor(target)

#         return image, target
class Compose_reversible(object):
    def __init__(self, transforms_before, transforms_after):
        self.transforms_before = transforms_before
        self.transforms_after = transforms_after

    def __call__(self, image, target):
        for t in self.transforms_before:
            image, target = t(image, target)
        random_flip_ = random_flip_r()
        image, target, flip = random_flip_(image, target)
        random_rotate_ = random_rotate_r((-180,180))
        image, target, angle = random_rotate_(image, target)
        for t in self.transforms_after:
            image, target = t(image, target)
        return image, target, (flip, angle)


class Compose(object):
    def __init__(self, transforms):
        self.transforms = transforms

    def __call__(self, image, target):
        for t in self.transforms:
            image, target = t(image, target)

        return image, target

class to_np(object):

    def __init__(self):
        pass

    def __call__(self, image, target):
        image = np.array(image)
        target = np.array(target)
        # print('img:', image.shape)
        # print('tar:', target.shape)
        return image, target

class convert_mask(object):

    def __init__(self):
        pass

    def __call__(self, image, target):
        target[target>0]=1
        target[target<=0]=0
        return image, target

class permute_dims(object):

    def __init__(self):
        pass

    def __call__(self, image, target):
        image = np.moveaxis(image,[0,1],[1,2])
        #target = np.moveaxis(target,[0,1],[1,2])
        return image, target
#torchvision.transforms.CenterCrop(size)

class c_crop(object):

    def __init__(self, size):
        self.crop = torchvision.transforms.CenterCrop(size)

    def __call__(self, image, target):
        image = self.crop(image)
        target = self.crop(target)
        return image, target

class to_tensor(object):
    def __init__(self, inp_dtype=torch.float32 , targ_dtype=torch.long):
        self.inputs_dtype = inp_dtype
        self.targets_dtype = targ_dtype
    def norm(self, inp):
        if torch.min(inp) == torch.max(inp):
            out = inp / torch.max(inp)
        else:
            out = (inp-torch.min(inp))/(torch.max(inp)-torch.min(inp))
        return out
    def __call__(self, x, y):
        # x, y = torch.from_numpy(x.astype(int)).type(self.inputs_dtype), \
        #         torch.from_numpy(y.astype(int)).type(self.targets_dtype)
        x, y = torch.from_numpy(x).type(self.inputs_dtype), \
                torch.from_numpy(y).type(self.targets_dtype)

        return self.norm(x), y

class three_channels(object):

    def __init__(self):
        pass

    def __call__(self, image, target):
        image = image.expand(3,*image.shape[1:])
        return image, target

class unsqueeze_tensor(object):

    def __init__(self, target=False):
        self.target = target

    def __call__(self, image, target):

        image = image.unsqueeze(0)
        if self.target:
            target = target.unsqueeze(0)

        return image, target


class reshape_target(object):

    def __init__(self, n_classes=2):
        self.n_classes = n_classes

    def __call__(self, image, target):
        target = torch.squeeze(target)
        # target[target<0] = 0
        # target[target>0] = 1
        try:
            target = F.one_hot(target ,num_classes=self.n_classes) \
                                                        .permute(2,0,1)
        except:
            print('max: ', torch.amax(target))
            print('all: ', torch.unique(target))
            # should be a more specific exception.
            raise Exception("Class values must be smaller than n_classes.")
        return image, target
class to_pil(object):

    def __init__(self):
        self.topil = torchvision.transforms.ToPILImage()

    def __call__(self, image, target):
        image = self.topil(image.astype('float'))
        target = self.topil(target.astype('float'))
        return image, target

class attenuation_channels(object):

    def __init__(self, offset=0, l1=None, w1=None, l2=None, w2=None,
                                                     l3=None, w3=None):
        """
        Make a three channel image out of a grayscale image by concatenating
        the original image 3 times and windowing it using different parameters
        for each.

        Parameters
        ----------
        offset : int, offset for the levels in HU. In case the images value
                        of 0 is not at 0 HU.
            DESCRIPTION. The default is 0.
        l1 : int, level for the first layer
            DESCRIPTION. The default is None.
        w1 : int, window width for layer 1. Window [l1-(w1/2), l1+(w1/2)]
            DESCRIPTION. The default is None.
        etc.

        Returns
        -------
        None.

        """
        if l1 == None: #put into fn definition!
            # normal renal attenuation range (−110 to 190 HU)
            l1 = 40
            w1 = 300
        if l2 == None:
            # high attenuation range (20-120 HU)
            l2 = 70
            w2 = 100
        if l3 == None:
            # low attenuation range (−40 to 60 HU).
            l3 = 10
            w3 = 50
        self.win1 = window(l1 + offset, w1)
        self.win2 = window(l2 + offset, w2)
        self.win3 = window(l3 + offset, w3)
    def __call__(self, image, target):
        im1, _ = self.win1(image, None)
        im2, _ = self.win2(image, None)
        im3, _ = self.win3(image, None)

        image = np.stack((im1,im2,im3))
        return image, target


class window(object):

    def __init__(self, lvl, width):
        self.level = lvl
        self.width = width
    def cut (self,image, level, width=256):
        MAX_IMAGE_VALUE = 255
        image[image < level - width/2] = level - width/2
        image[image >=  level + width/2] = level + width/2
        min_val = np.min(image)
        max_val = np.max(image)


        if max_val == min_val:
            im_norm = image /max_val
        else:
            im_norm = (image - min_val) / (max_val - min_val)

        im_bt = im_norm * MAX_IMAGE_VALUE
        # im_bt = im_bt.astype('uint8')
        return im_bt
    def __call__(self, image, target):
        image = self.cut(image, self.level, self.width)
        return image, target
