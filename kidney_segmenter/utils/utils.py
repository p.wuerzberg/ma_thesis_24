# -*- coding: utf-8 -*-
"""
Created on Sun Feb 11 15:41:12 2024

@author: Paul
"""
import os
import shutil
import fastprogress
import time
from pathlib import Path

import numpy as np
import matplotlib.pyplot as plt
import torch
# from torch import nn
import torch.nn.functional as F
# import seaborn as sns

from scipy.spatial import distance
from sklearn.metrics import confusion_matrix as cfmat

def get_device(cuda_preference=True):
    """Gets pytorch device object. If cuda_preference=True and
        cuda is available on your system, returns a cuda device.

    Args:
        cuda_preference: bool, default True
            Set to true if you would like to get a cuda device

    Returns: pytorch device object
            Pytorch device
    """

    print('cuda available:', torch.cuda.is_available(),
          '; cudnn available:', torch.backends.cudnn.is_available(),
          '; num devices:', torch.cuda.device_count())

    use_cuda = False if not cuda_preference else torch.cuda.is_available()
    device = torch.device('cuda:0' if use_cuda else 'cpu')
    device_name = torch.cuda.get_device_name(device) if use_cuda else 'cpu'
    print('Using device', device_name)
    return device


def predict(model, device, dataloader):
    with torch.no_grad():
        out_list = []
        gt_list =[]
        orig_list = []
        smax = torch.nn.Softmax(dim=1)

        model.eval()
        for img, y in fastprogress.progress_bar(dataloader):
            orig_list.append(img)
            img = img.to(device)
            output = model(img)
            output = list(output.items())[0][1]#
            output = smax(output)
            output = torch.argmax(output, dim=1)

            output = F.one_hot(output, num_classes=3)

            output = output.cpu().numpy()
            out_list.append(output)
            gt_list.append(y)


def train(dataloader, optimizer, model, loss_fn, device, master_bar):
    """Run one training epoch.

    Args:
        dataloader (DataLoader): Torch DataLoader object to load data
        optimizer: Torch optimizer object
        model (nn.Module): Torch model to train
        loss_fn: Torch loss function
        device (torch.device): Torch device to use for training
        master_bar (fastprogress.master_bar): Will be iterated over for each
            epoch to draw batches and display training progress

    Returns:
        float, float: Mean loss of this epoch, fraction of correct predictions
            on training set (accuracy)
    """
    epoch_loss = []
    # epoch_correct, epoch_total = 0, 0
    dice = 0
    count = 0
    for x, y in fastprogress.progress_bar(dataloader, parent=master_bar):
        optimizer.zero_grad()
        model.train()
        y_idx = torch.argmax(y, dim=1)

        y_idx = y_idx.to(device)
        # Forward pass
        y_pred = model(x.to(device))
        y_pred = list(y_pred.items())[0][1]

        loss = loss_fn(y_pred, y.to(device).float())

        # Backward pass
        loss.backward()
        optimizer.step()
        y_pred_idx = torch.argmax(y_pred.cpu(), dim=1).numpy()

        y_idx = y_idx.cpu().flatten().numpy()
        if np.max(y_idx) == 0:
            print("ground truth == 0 during training")
        dice += 1-distance.dice(y_pred_idx.flatten(), y_idx)
        count += 1
        # For plotting the train loss, save it for each sample
        epoch_loss.append(loss.item())

    # Return the mean loss and the accuracy of this epoch
    dice = dice/count
    print(f"Training loss: {np.mean(epoch_loss)} Dice: {dice}")
    return np.mean(epoch_loss), dice


def validate(dataloader, model, loss_fn, device, master_bar):
    """Compute loss, accuracy and confusion matrix on validation set.

    Args:
        dataloader (DataLoader): Torch DataLoader object to load data
        model (nn.Module): Torch model to train
        loss_fn: Torch loss function
        device (torch.device): Torch device to use for training
        master_bar (fastprogress.master_bar): Will be iterated over to draw
            batches and show validation progress

    Returns:
        float, float, torch.Tensor shape (10,10): Mean loss on validation set,
            fraction of correct predictions on validation set (accuracy)
    """
    epoch_loss = []
    epoch_correct, epoch_total = 0, 0
    confusion_matrix = torch.zeros(3, 3)    #tp fp \ fn tn
    dice = 0
    count = 0
    model.eval()
    with torch.no_grad():
        for x, y in fastprogress.progress_bar(dataloader, parent=master_bar):
            # make a prediction on validation set
            y_pred = model(x.to(device))
            y_pred = list(y_pred.items())[0][1]
            y_idx = torch.argmax(y, dim=1)
            if torch.max(y[:,0,:,:]) ==torch.min(y[:,0,:,:]):
                print('original inp',torch.max(y[:,0,:,:]))
                print('original inp',torch.min(y[:,0,:,:]))

            y_idx = y_idx.to(device)
            y_pred_idx = torch.argmax(y_pred, dim=1)

            loss = loss_fn(y_pred, y.to(device).float())

            epoch_correct += (y_idx == y_pred_idx).sum()
            epoch_total += y.shape[0] * y.shape[2] * y.shape[3]
            y_pred_idx = y_pred_idx.cpu().flatten().numpy()
            y_idx = y_idx.cpu().flatten().numpy()

            if np.max(y_idx) == 0 and np.max(y_pred_idx) == 0:
                print('np.max(y_idx) == 0 and np.max(y_pred_idx) == 0')
            if np.max(y_idx) == 0:
                print('gt==0 wtf ', np.unique(y_idx))

            np.seterr(all='raise')
            try:
                dice += 1-distance.dice(y_pred_idx, y_idx)
            except:

                print('dice fup')
                print(f"gt: {y_idx.shape}, {y_idx.dtype}, pre: {y_pred_idx.shape},\
                      \n {y_pred_idx.dtype}")
                print(f"gt min {np.min(y_idx)}")
                print(f"gt max {np.max(y_idx)}")
                print(f"pr min {np.min(y_pred_idx)}")
                print(f"pr max {np.max(y_pred_idx)}")
            count += 1

            # Fill confusion matrix
            batch_cmat = cfmat(y_idx, y_pred_idx, labels=[0,1,2])

            confusion_matrix += batch_cmat
#
            # For plotting the train loss, save it for each sample
            epoch_loss.append(loss.item())
    print(f"# of all val iterations {count} ")
    dice = dice/count
    print("VALIDATE")
    print("loss: " + str(np.mean(epoch_loss)))
    print("correct: " + str(epoch_correct))
    print("total: " + str(epoch_total))
    print(f"DICE: {dice}")

    return np.mean(epoch_loss), dice, confusion_matrix


def run_training(model, optimizer, loss_function, device, num_epochs,
                train_dataloader, val_dataloader, early_stopper=None,
                 verbose=False, scheduler = None, dynamic_lr=None, stats=None,
                 savepath='/content/gdrive/My Drive/kidney-unet2-data/results/temp'):
    """Run model training.

    Args:
        model (nn.Module): Torch model to train
        optimizer: Torch optimizer object
        loss_fn: Torch loss function for training
        device (torch.device): Torch device to use for training
        num_epochs (int): Max. number of epochs to train
        train_dataloader (DataLoader): Torch DataLoader object to load the
            training data
        val_dataloader (DataLoader): Torch DataLoader object to load the
            validation data
        early_stopper (EarlyStopper, optional): If passed, model will be trained
            with early stopping. Defaults to None.
        verbose (bool, optional): Print information about model training.
            Defaults to False.

    Returns:
        list, list, list, list, torch.Tensor shape (10,10): Return list of train
            losses, validation losses, train accuracies, validation accuracies
            per epoch and the confusion matrix evaluated in the last epoch.
    """
    start_time = time.time()
    master_bar = fastprogress.master_bar(range(num_epochs))
    train_losses, val_losses, train_accs, val_accs = [],[],[],[]
    if stats is not None:
        train_losses, val_losses, train_accs, val_accs = stats.tolist()
    confusion_matrix = []
    iterator = (i for i in range(num_epochs))
    for epoch in master_bar:
        # Train the model
        epoch_train_loss, epoch_train_acc = train(train_dataloader, optimizer,
                                                  model, loss_function,
                                                  device, master_bar)
        # Validate the model
        epoch_val_loss, epoch_val_acc, confusion = validate(val_dataloader,
                                                        model, loss_function,
                                                        device, master_bar)
        save_model(model, Path(savepath, 'tmp_weights.txt'))
        if isinstance(scheduler, torch.optim.lr_scheduler.ReduceLROnPlateau):
            scheduler.step(epoch_val_acc)

        else:
            if scheduler is not None:
                scheduler.step()
        if isinstance(scheduler, restartLR) and scheduler.lowpoint:
            save_model(model, Path(savepath, str(next(iterator)) + 'weights.txt'))
            print("saving", scheduler.test)
        # Save loss and acc for plotting
        train_losses.append(epoch_train_loss)
        val_losses.append(epoch_val_loss)
        train_accs.append(epoch_train_acc)
        val_accs.append(epoch_val_acc)
        confusion_matrix.append(confusion)

        np.save( Path(savepath, 'stats.npy'),
                np.array([train_losses, val_losses, train_accs, val_accs]))
        if verbose:
            master_bar.write(f'Train loss: {epoch_train_loss:.2f}, val loss: {epoch_val_loss:.2f}, train acc: {epoch_train_acc:.3f}, val acc {epoch_val_acc:.3f}')

        if dynamic_lr:
            dynamic_lr.update(epoch_val_acc, model)
            if dynamic_lr.change:
                for g in optimizer.param_groups:
                    g['lr'] = g['lr']/10
                print("decreasing learning rate")
                dynamic_lr.reset()

        if early_stopper:

            early_stopper.update(epoch_val_acc, model)
            if early_stopper.early_stop:
                return train_losses, val_losses, train_accs, val_accs, \
                                                        confusion_matrix


    time_elapsed = np.round(time.time() - start_time, 0).astype(int)
    print(f'Finished training after {time_elapsed} seconds.')
    return train_losses, val_losses, train_accs, val_accs, confusion_matrix


def plot(title, label, train_results, val_results, yscale='linear', save_path=None,
         extra_pt=None, extra_pt_label=None):
    """Plot learning curves.

    Args:
        title (str): Title of plot
        label (str): x-axis label
        train_results (list): Results vector of training of length of number
            of epochs trained. Could be loss or accuracy.
        val_results (list): Results vector of validation of length of number
            of epochs. Could be loss or accuracy.
        yscale (str, optional): Matplotlib.pyplot.yscale parameter.
            Defaults to 'linear'.
        save_path (str, optional): If passed, figure will be saved at this path.
            Defaults to None.
        extra_pt (tuple, optional): Tuple of length 2, defining x and y coordinate
            of where an additional black dot will be plotted. Defaults to None.
        extra_pt_label (str, optional): Legend label of extra point. Defaults to None.
    """

    epoch_array = np.arange(len(train_results)) + 1
    train_label, val_label = "Training "+label.lower(), "Validation "+label.lower()

    # sns.set(style='ticks')

    plt.plot(epoch_array, train_results, epoch_array, val_results, linestyle='dashed', marker='o')
    legend = ['Train results', 'Validation results']

    if extra_pt:

        plt.plot(*extra_pt,'.k')
        #raise NotImplementedError



    plt.legend(legend)
    plt.xlabel('Epoch')
    plt.ylabel(label)
    plt.yscale(yscale)
    plt.title(title)

    # sns.despine(trim=True, offset=5)
    plt.title(title, fontsize=15)
    if save_path:
        plt.savefig(str(save_path), bbox_inches='tight')
    plt.show()


def save_model(model, path):
       """Save model checkpoint.

       Args:
           model (nn.Module): Model of which parameters should be saved.
           path (string): savepath
       """

       torch.save(model.state_dict(), path)

def load_model(model, path):
    """
    Loads previously saved weights for the passed model.

    Parameters
    ----------
    model : (nn.Module):
        Model for which parameters should be loaded
    path : String
        Path to model weights.

    Returns
    -------
    model : nn.Module
        The model with the loaded weights.

    """

    model.load_state_dict(torch.load(path))
    return model

class restartLR:
    """decrease the learning rate at a regular intervall
    """
    def __init__(self, optimizer, step_size=1,cycle = 10, gamma = .1):
        """Initialization.
         Args:
            optimizer (torch.optim.Optimizer): the used optimizer
            step_size (int): number of steps before decrease
            gamma (float): (0,1) decrease lr rate
        """
        self.step_size = step_size
        self.counter = 0
        self.cycle_counter = 0
        self.optimizer = optimizer
        self.gamma = gamma
        self.cycle = cycle
        self.lr = optimizer.param_groups[0]['lr']
        self.base_lr = self.lr
        self.test = 0


        self.__lowpoint = False

    def step(self):
        """Call after one epoch of model training to update early stopper object.
         """
        self.counter += 1
        self.test += 1
        if self.counter >= self.step_size:
            self.cycle_counter += 1
            if self.cycle_counter >= self.cycle:
                self.lr = self.base_lr
                self.cycle_counter = 0
            else:
                self.lr *= self.gamma
            print(self.lr)
            for g in self.optimizer.param_groups:
                g['lr'] = self.lr
            self.counter = 0
        return self.counter

    @property
    def lowpoint(self):
        """True if early stopping criterion is reached.

        Returns:
            [bool]: True if early stopping criterion is reached.
        """

        if self.cycle_counter == 0 and self.counter == 0:
            self.__lowpoint = True
        else:
            self.__lowpoint = False
        return self.__lowpoint


class EarlyStopper:
    """Early stops the training if validation accuracy does not increase after a
    given patience.
    """
    def __init__(self, verbose=False, path='checkpoint.pt', patience=1):
        """Initialization.

        Args:
            verbose (bool, optional): Print additional information. Defaults to False.
            path (str, optional): Path where checkpoints should be saved.
                Defaults to 'checkpoint.pt'.
            patience (int, optional): Number of epochs to wait for increasing
                accuracy. If accyracy does not increase, stop training early.
                Defaults to 1.
        """
        self.patience = patience
        self.verbose = verbose
        self.counter = 0
        self.best_acc = None
        self.__early_stop = False
        self.val_acc_max = -np.Inf
        self.path = path


    @property
    def early_stop(self):
        """True if early stopping criterion is reached.

        Returns:
            [bool]: True if early stopping criterion is reached.
        """

        if self.counter >= self.patience:
            self.__early_stop = True
        return self.__early_stop


    def save_checkpoint(self, model, val_acc):
        """Save model checkpoint.

        Args:
            model (nn.Module): Model of which parameters should be saved.
        """
        if self.verbose:
            print(f'Validation accuracy increased ({self.val_acc_max:.6f} --> {val_acc:.6f}).  Saving model ...')

        torch.save(model.state_dict(), self.path)

    def update(self, val_acc, model):
        """Call after one epoch of model training to update early stopper object.

        Args:
            val_acc (float): Accuracy on validation set
            model (nn.Module): torch model that is trained
        """

        if val_acc <= self.val_acc_max:
            self.counter += 1
        else:
            self.counter = 0
            self.val_acc_max = val_acc
            self.save_checkpoint(model, val_acc)
        #return early_stop()




    def load_checkpoint(self, model):
        """Load model from checkpoint.

        Args:
            model (nn.Module): Model that should be reset to parameters loaded
                from checkpoint.

        Returns:
            nn.Module: Model with parameters from checkpoint
        """
        if self.verbose:
            print(f'Loading model from last checkpoint with validation accuracy {self.val_acc_max:.6f}')

        model.load_state_dict(torch.load(self.path))
        return model

def clear_outdir(out):
    """
    Warning: deletes files without further warning.
    Delete everything below 'out'.

    Parameters
    ----------
    out : str or pathlib.Path
        Path to dir that is to be emptied. Delete out/*

    Returns
    -------
    None.

    """
    for root, dirs, files in os.walk(out):
        for f in files:
            os.unlink(os.path.join(root, f))
        for d in dirs:
            shutil.rmtree(os.path.join(root, d))