# -*- coding: utf-8 -*-
"""
Created on Sun Feb 11 15:48:14 2024

@author: Paul
"""

import os
from pathlib import Path
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import torch
import torch.nn as nn

import kidney_segmenter.utils.utils as utils
from kidney_segmenter.segmenter import premade
from kidney_segmenter.utils import transforms
from kidney_segmenter.utils import list_data
from kidney_segmenter.utils.tumordataset import SegmentationDataSet
from kidney_segmenter.utils.losses import DiceLoss






def create_dataset(path_train, path_val):
    val_path_im, val_path_ma = path_val / 'img', path_train / 'mask'
    path_im, path_ma = path_train / 'img', path_train / 'mask'
    val_images, val_targets = list_data.list_img_mask(val_path_im, val_path_ma,
                                                      sfx = '.png')

    images, targets = list_data.list_img_mask(path_im, path_ma,
                                              sfx = '.png')

    return (images, targets), (val_images, val_targets)

def create_dataset_thesis(PV_path, ART_path):
    valart_path = ART_path / "val"
    trainart_path = ART_path / "train"
    valpv_path = PV_path / "val"
    trainpv_path = PV_path / "train"
    val_images, val_targets = list_data.get_data_pv_art(valpv_path, valart_path,
                                                        sfx = '.png')

    images, targets = list_data.get_data_pv_art(trainpv_path, trainart_path,
                                                sfx = '.png')

    return (images, targets), (val_images, val_targets)




def training(train, val, tr_transform, val_transform, Parameters, out='./out'):

    root_out_path = Path(out)
    device = utils.get_device()
    # transform = transforms.transform()
    pre_transform = transforms.pre_transform(Parameters['size'])

    val_images, val_targets = val
    val_set = SegmentationDataSet(val_images, val_targets, use_cache = False,
                            pre_transform=pre_transform,
                            transform=val_transform)

    images, targets = train
    data_set = SegmentationDataSet(images, targets, use_cache = False,
                                pre_transform=pre_transform,
                                transform=tr_transform)


    val_loader = torch.utils.data.DataLoader(val_set,
                                    batch_size = Parameters['val_batch_size'])

    train_loader = torch.utils.data.DataLoader(data_set,
                        batch_size = Parameters['batch_size'], shuffle=True)

    for run in range(0, Parameters['train_n_weights']):
        out_path = root_out_path / str(run)
        os.makedirs(Path(out_path), exist_ok=True)
        """# Load Model"""

        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False


        model = premade.model_deeplab()
        model = nn.DataParallel(model)
        model.to(device)


        lr = Parameters['lr']
        epochs = Parameters['epochs']

        optimizer = torch.optim.SGD(model.parameters(), lr=lr, momentum=0.9)

        loss_function = DiceLoss()

        earlystopper = utils.EarlyStopper(patience=Parameters['patience'])

        train_losses, val_losses, train_accs, val_accs, cmat = utils.run_training(
                model, optimizer, loss_function, device, epochs, train_loader,
                val_loader,savepath= out_path, early_stopper=earlystopper)


        dot = (np.argmax(val_accs)+1,np.max(val_accs))

        utils.plot(str(lr),'loss', train_losses,val_losses,
                   save_path=out_path / 'losses.png')
        plt.clf()
        utils.plot(str(lr),'dice', train_accs,val_accs,extra_pt=dot,
                   extra_pt_label=str(dot), save_path=out_path / 'dice.png')


        # Load best weights from early stopper
        model = earlystopper.load_checkpoint(model)

        utils.save_model(model, Path(out_path, 'weights.txt'))


if __name__ == "__main__":
    root_out_path = Path('./out')
    PV_path = Path('./data/datasets/processed_PV/full')
    ART_path = Path('./data/datasets/processed_ART/full')

    Parameters={
        'Name': 'segmenter',
        'seed': 42,
        'batch_size': 64, # High numbers that dont fit the gpu give strange errors
        'val_batch_size': 64,
        'epochs': 3,
        'patience': 30,
        'lower_lr_after': 15,
        'train_n_weights': 1,
        'cycles': 1,
        'lr': .01,
        # 'size': (512,512),
        'size': (64,64),
        'Description': 'deeplab w dice loss',
        'date': str(datetime.now())
        }

    tr_transform = transforms.Compose([
                        transforms.to_np(),
                        # transforms.window(40+1024, 400), #L:40 +1024 offset
                        transforms.convert_mask(),
                        transforms.to_tensor(),
                        transforms.unsqueeze_tensor(target=True),
                        transforms.random_adjust_sharpness(),
                        transforms.random_elastic(),
                        transforms.random_flip(),
                        transforms.random_rotate((0,180)),
                        transforms.AddGaussianBlurr(),
                        transforms.reshape_target(),
                        transforms.three_channels()
                        ])

    val_transform = transforms.Compose([
                        transforms.to_np(),
                        # transforms.window(40+1024, 400),
                        transforms.convert_mask(),
                        transforms.to_tensor(),
                        transforms.unsqueeze_tensor(),
                        transforms.reshape_target(),
                        transforms.three_channels()
                        ])

    train, val = create_dataset_thesis(PV_path, ART_path)
    training(train, val, tr_transform, val_transform, Parameters)