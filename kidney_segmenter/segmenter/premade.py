# -*- coding: utf-8 -*-
"""
Created on Tue Mar 28 12:38:18 2023

@author: Paul
"""

import torch

from torchvision.models.segmentation import fcn_resnet50, FCN_ResNet50_Weights
from torchvision.models.segmentation import deeplabv3_resnet50
# from torchvision.models.segmentation import DeepLabV3_ResNet50_Weights


def model_unet(out_dim=2):
    model = torch.hub.load('mateuszbuda/brain-segmentation-pytorch', 'unet',
    in_channels=3, out_channels=1, init_features=32, pretrained=True)
    model.conv = torch.nn.Conv2d(32,out_dim, 1,1)
    return model


def model_deeplab(out_dim=2):
    # weights = DeepLabV3_ResNet50_Weights.DEFAULT
    model= deeplabv3_resnet50(weights=None)
    # torch.nn.init.normal_(model.weight, mean=0, std=1)
    model.aux_classifier = None
    model.classifier[4] = torch.nn.Conv2d(256, out_dim, 1, 1)
    return model


def model_fcn():
    weights = FCN_ResNet50_Weights.COCO_WITH_VOC_LABELS_V1
    model = fcn_resnet50(weights=weights)
    model.aux_classifier = None
    model.classifier[4] = torch.nn.Conv2d(512, 2, 1, 1)
    return model